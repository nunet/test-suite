## CLI

The `cli.sh` script provides a unified interface for managing LXD infrastructure and running functional tests.

### Basic Usage
```bash
./cli.sh <context> <action> [additional arguments...]
```

### Customization via environment variables

The following are some of the the environment variables available for customizing test
execution via the CLI:

- `BEHAVE_STEP_TIMEOUT`: sets the timeout for each step when running gherking features in
  python-behave. Default: `30`
- `FEATURE_ENVIRONMENT_DOCKER`: Set to "false" to run tests directly without Docker
container (default: "true")
- `BEHAVE_ENABLE_ALLURE`: Set to "true" to enable Allure test reporting (default: "false") 
- `BEHAVE_ENABLE_JUNIT`: Set to "true" to enable JUnit test reporting (default: "false")
- `BEHAVE_VERBOSE_VALIDATION`: Set to "true" to enable verbose validation of feature
files, which displays among other things skipped steps (default: "false")

### LXD Infrastructure Management
See [LXD Usage Guide](infrastructure/dms-on-lxd/USAGE.md) for detailed information.

```bash
# Create LXD infrastructure
./cli.sh lxd create

# Destroy LXD infrastructure
./cli.sh lxd destroy
```

### Functional Tests
See [Functional Tests Guide](stages/functional_tests/GUIDE.md) for detailed information.

```bash
# Run standalone tests
./cli.sh functional-tests standalone

# Run distributed tests
./cli.sh functional-tests distributed

# List available tests
./cli.sh functional-tests list

# Run specific distributed tests
./cli.sh functional-tests distributed $(./cli.sh functional-tests list | grep actor | grep distributed)

# Run specific standalone tests
./cli.sh functional-tests standalone $(./cli.sh functional-tests list | grep actor | grep standalone)
```

