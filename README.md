# test-suite

This repository combines all tests and defines full testing pipeline of the platform. The test pipeline is rather complex and different stages are implemented in different repositories. This repository aims at holding all relevant information and definitions inrrespectively as well as provide the interface to test management framework, which displays results of each test run (see https://nunet.testmo.net/). 

## Repository structure

```
├── .gitlab         # issue templates and other technical gitlab related artifacts
├── stages          # definitions of use cases and bahaviors of each test stage
├── environments    # definitions of environments
```

## Testing matrix

### Pre-build tests

| Stage | Feature | Staging | Status |
|-------|---------|---------|--------|
| code_quality          | y | y | implemented |
| unit_tests            | y | y | implemented |
| compliance_check      | y | y | not implemented |
| integration_tests     | y | y | disabled |
| end_to_end_tests      | y | y | implemented |
| security_tests        | y | y | implemented |

### Tests running in testnet environments

| Stage | Feature | Staging | Status |
|-------|---------|---------|--------|
| functional_tests       | y | n | implemented |
| integration_tests_live | y | n | not implemented |
| security_tests_live    | y | n | not implemented |
| regression_tests       | n | y | not implemented |
| performance_tests      | n | y | not implemented |
| load_tests             | n | y | not implemented |
| security_tests_3       | n | y | not implemented |

### Tests running in production
TBD

How to read the table: columns are environments and rows are test stages; there is a `build` stage that is ommited and runs between pre-build tests and testnet environments:

* `y` means that a corresponding environment is needed to run a stage (equivalent -- a stage runs in that environment); 
* `n` means the opposite -- a stage does not need that environment to run an is not executed in that environment;

The meaning of the testing matrix cells is explained below. Note, they may differ depending on the stage (e.g. manual execution of deploy stage means that at least part of the environment will need manual actions from community compute providers; likewise, manual acceptance tests consider involvement of beta testers). Furthermore, some test stages contain sub-stages and the whole matrix will be evolving together with the code base.

## Branching strategy

The branching strategy is defined [here](https://gitlab.com/nunet/team-processes-and-guidelines/-/tree/main/git_workflows#git-workflow---branching-strategy).

## Testing schedule

TBD: will define when and how we run advanced tests which need manual testing as well as heavy environments (we will not be able to run them very frequently, and that will need to be preperly scheduled).

