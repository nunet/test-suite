include:
  - local: cicd/Jobs/Publish-Reports.gitlab-ci.yml
  - local: cicd/Jobs/templates/go.gitlab-ci.yml
  - local: cicd/Jobs/templates/testmo.gitlab-ci.yml

.base_unit_tests:
  stage: unit_tests
  allow_failure:
    exit_codes: 100
  dependencies: []
  needs: []

Golang Unit Tests:
  dependencies: []
  needs: []
  extends:
    - .base_unit_tests
    - .go-base
    - .base_golang_gotestsum

Golang Coverage Report:
  extends: 
    - .base_unit_tests
    - .go-base
  needs:
    - job: Golang Unit Tests
      optional: true
  dependencies:
    - Golang Unit Tests
  coverage: '/^coverage: (\d+.\d+)% of statements$/'
  image: golang:1.23
  script:
    - echo "CODE_COVERAGE_JOB_URL=${CI_JOB_URL}" >> vars.env
    - go get github.com/boumenot/gocover-cobertura@v1.3.0
    - go run github.com/boumenot/gocover-cobertura < coverage.txt > coverage.xml
    - go tool cover -html coverage.txt -o coverage.html
    - |
      echo coverage: $(go tool cover -func=coverage.txt | grep total: | awk '{print $3}') of statements
  when: always
  artifacts:
    reports:
      dotenv: vars.env      
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - coverage.html
      - coverage.xml

JavaScript Unit Tests:
  extends: .base_unit_tests
  dependencies: []
  needs: []
  allow_failure:
    exit_codes: 100
  before_script: npm install --global mocha
  script:
    - echo "UNIT_TEST_JOB_URL=${CI_JOB_URL}" >> vars.env
    - |
      if ! mocha --reporter mocha-junit-reporter --reporter-options mochaFile=junit.xml; then
        if [[ $ALLOW_UNIT_TESTS_FAIL == "true" ]]; then
          exit 100
        else
          exit 1
        fi
      fi
  artifacts:
    when: always
    reports:
      dotenv: vars.env
      junit: junit.xml
    paths:
      - junit.xml
  rules:
    - if: $SKIP_UNIT_TESTS
      when: never
    - exists:
        - package.json
        - tests/*
      if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - package.json
        - tests/*

JavaScript Coverage Report:
  extends: .base_unit_tests
  needs:
    - job: JavaScript Unit Tests
      optional: true
  dependencies:
    - JavaScript Unit Tests
  rules:
    - if: $SKIP_UNIT_TESTS
      when: never
    - exists:
        - package.json
        - tests/*
      if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
      exists:
        - package.json
        - tests/*
  script:
    - echo "CODE_COVERAGE_JOB_URL=${CI_JOB_URL}" >> vars.env
    - npm install
    - npx nyc --reporter cobertura mocha
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml

"publish_reports:coverage_html":
  extends: 
    - .go-base
    - .publish_reports
  dependencies:
    - "Golang Coverage Report"
    - "JavaScript Coverage Report"
  needs:
    - job: Golang Coverage Report
      artifacts: true
      optional: true
    - job: "JavaScript Coverage Report"
      artifacts: true
      optional: true
  variables:
    REPORTS_TO_UPLOAD: coverage.html

"upload_testmo:unit_tests":
  extends:
    - .testmo-upload
  when: always
  needs:
    - job: Golang Unit Tests
      optional: true
    - job: JavaScript Unit Tests
      optional: true
    - job: Golang Coverage Report
      optional: true
    - job: JavaScript Coverage Report
      optional: true
    - job: golangci_lint
      optional: true
    - job: "publish_reports:coverage_html"
      artifacts: true
  allow_failure: true
  dependencies:
    - Golang Unit Tests
    - JavaScript Unit Tests
    - Golang Coverage Report
    - JavaScript Coverage Report
    - golangci_lint
    - "publish_reports:coverage_html"
  rules:
    - if: $SKIP_UNIT_TESTS
      when: never
    - when: always
  script:
    - |
      echo "Adding links to Testmo"
      
      testmo automation:resources:add-link --name "Job ID" --url "$CI_JOB_URL" --note "Job ID of the CI Pipeline." --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json"
      
      testmo automation:resources:add-link --name "Commit ID" --url "${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}" --note "Commit ID of the CI Pipeline." --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json"
      
      testmo automation:resources:add-link --name "Pipeline ID" --url "$CI_PIPELINE_URL" --note "Commit ID of the CI Pipeline." --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json" 

      testmo automation:resources:add-link --name "Gitlab Test Reports" --url "$CI_PROJECT_URL/-/pipelines/$CI_PIPELINE_ID/test_report" --note "Test reports as presented in Gitlab Merge Requests" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json" 

      echo "${CI_REPORTS_FINAL_URI:?err no reports final uri}/coverage.html"
      testmo automation:resources:add-link --name "Coverage HTML" --url "${CI_REPORTS_FINAL_URI:?err no reports final uri}/coverage.html" --note "Go coverage report in HTML format" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json" 
    - |
      echo "Adding artifacts"

      testmo automation:resources:add-artifact --name "Job Log" --url "${UNIT_TEST_JOB_URL}/artifacts/download?file_type=trace" --note "Trace logs of the job" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json" 

      testmo automation:resources:add-artifact --name "Unit Tests JUnit Results" --url "${UNIT_TEST_JOB_URL}/artifacts/download?file_type=junit" --note "JUnit XML of the job" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json" 

      testmo automation:resources:add-artifact --name "Cobertura Coverage" --url "${CODE_COVERAGE_JOB_URL}/artifacts/download?file_type=cobertura" --note "Cobertura Coverage of the job" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json"  

    - |
      if [ -f golangci-lint.html ]; then
        testmo automation:resources:add-link --name "Golangci-lint Report" --url "${CI_REPORTS_FINAL_URI:?err no reports final uri}/golangci-lint.html" --note "Golang-ci report in HTML format" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json" 

        testmo automation:resources:add-artifact --name "Golangci-lint junit report" --url "${GOLANGCI_LINT_JOB_URL}/artifacts/download?file_type=junit" --note "Golangci-lint junit report" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json"  
      fi 
    - |
      echo "Sending Unit Tests Results"

      if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then
          export TESTMO_CONFIG="development";
      elif [[ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" =~ ^release ]]; then
          export TESTMO_CONFIG="staging"
      elif [[ "$CI_COMMIT_BRANCH" =~ ^release ]]; then
          export TESTMO_CONFIG="production"
      elif [[ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" == "main" ]]; then
          export TESTMO_CONFIG="feature"
      else
          echo "No conditions matched. Setting TESTMO_CONFIG to default value."
          export TESTMO_CONFIG="feature"
      fi

      echo "TESTMO_CONFIG is set to: $TESTMO_CONFIG"
      echo "TESTMO_SOURCE is set to: $TESTMO_SOURCE"
      
      testmo automation:run:submit --instance $TESTMO_URL --project-id $TEST_PROJECT_ID --source "unittests" --results junit.xml --name "Unit-Test" --source "$TESTMO_SOURCE" --milestone "Platform Alpha" --tags "sha_$CI_COMMIT_SHORT_SHA" --resources "Unit-Test-Pipeline#${CI_PIPELINE_ID}.json" --config "$TESTMO_CONFIG"

update_code_coverage:
  stage: .post
  image:
    name: alpine/git:2.43.0
    entrypoint:
      - ""
  needs:
    - Golang Coverage Report
  script:
    # - sed -ie "s/badges\/.*\/coverage/badges\/$CI_COMMIT_BRANCH\/coverage/g" README.md
    - sed -ie "s~badges/.*/coverage~badges/main/coverage~g" README.md
    - git config --global user.email group_${CI_PROJECT_NAMESPACE_ID}_bot_cicd@noreply.${CI_SERVER_HOST}
    - git config --global user.name group_${CI_PROJECT_NAMESPACE_ID}_bot_cicd
    - git config --global --add safe.directory "$PWD"
    # add must be used on first run per runner, on subsequent runs error is caught
    - git remote add gitlab_origin https://${GITLAB_ACCESS_USER}:${GITLAB_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git || true
    - git remote set-url gitlab_origin https://${GITLAB_ACCESS_USER}:${GITLAB_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git
    - git add README.md
    #  don't fail if no changes
    - git commit -m "push back from pipeline - README.md" || true
    # prevent triggering pipeline again
    - git push gitlab_origin HEAD:$CI_COMMIT_BRANCH -o ci.skip || true
  rules:
  - when: never
    #  - if: $SKIP_UNIT_TESTS
    #    when: never
    #  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    #  - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
    #    when: never
    #  - if: $CI_COMMIT_BRANCH

