import json
import os
import re
import requests
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def fetch_mr_details(mr_id):
    """Fetches details of a specific merge request using the GitLab API."""
    logging.info("Determining change type")
    project_id = os.getenv('CI_PROJECT_ID')
    private_token=os.getenv('OUR_GITLAB_TOKEN')
    url = f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{mr_id}"
    headers = {"PRIVATE-TOKEN": private_token}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()  # Returns the MR details as a dictionary
    else:
        response.raise_for_status()  # Raises an HTTPError for bad responses

def get_change_type(description):
    """Extracts the type of change (major, minor, patch) from the MR description."""
    if "[x] This MR does not necessitate a change" in description:
        return "skip"
    elif "[x] This MR adds a new **feature**" in description:
        return "minor"
    elif "[x] This MR only includes **bug fixes or patches**" in description or "[x] fix: A bug fix (hot fix)" in description:
        return "patch"
    elif "[x] This MR introduces a **breaking change**." in description:
        return "major"
    else:
        return None

def identify_action(mr_details):
    """Identifies the action based on MR details and branching strategy."""
    logging.info("Identifying action based on MR details and branching strategy...")
    description = mr_details['description']
    target_branch = mr_details['target_branch']
    source_branch = mr_details['source_branch']
    action = get_change_type(description)
    if   action == "skip":
        return "skip"
    elif target_branch == "staging" and action == "patch":
        return ("hotfix_to_staging", action)
    elif target_branch == "develop":
        return ("direct_merge_to_develop", action) 
    elif source_branch == "develop" and target_branch == "staging":
        return ("merge_develop_to_staging", action)
    elif source_branch == "staging" and target_branch == "main":
        return ("merge_staging_to_main", action)
    else:
        logging.error("unable to identify this action got soruce branch:{source_branch} target branch: {target_branch}, action:{action}!")
        return None

def create_merge_request_to_develop():
    project_id = os.getenv('CI_PROJECT_ID')
    private_token = os.getenv('OUR_GITLAB_TOKEN')
    source_branch = 'staging'
    target_branch = 'develop'
    title = 'Merge Hotfix from Staging to Develop'
    description = 'Automated merge request to incorporate hotfixes back into the develop branch.'
    
    url = f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests"
    data = {
        'id': project_id,
        'source_branch': source_branch,
        'target_branch': target_branch,
        'title': title,
        'description': description,
        'remove_source_branch': True
    }
    headers = {'PRIVATE-TOKEN': private_token}
    
    response = requests.post(url, headers=headers, json=data)
    if response.ok:
        logging.info("Merge request created successfully.")
    else:
        logging.error(f"Failed to create merge request: {response.text}")

def get_current_version(project_id, private_token, language):
    """Fetches the current version of the project from the 'develop' branch."""
    file_path_map = {
        'go': 'main.go',
        'javascript': 'package.json',
        'python': 'setup.py'
    }
    file_path = file_path_map.get(language)
    if not file_path:
        logging.error("Unsupported language for version retrieval.")
        return None

    url = f"https://gitlab.com/api/v4/projects/{project_id}/repository/files/{file_path}/raw?ref=develop"
    headers = {"PRIVATE-TOKEN": private_token}
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200:
        if language == 'javascript':
            # For JavaScript, parse JSON and return the version key
            content = response.json()
            return content.get('version')
        elif language == 'go':
            # For Go, find the version line and extract the version number
            match = re.search(r'@version\s+(\d+\.\d+\.\d+)', response.text)
            return match.group(1) if match else None
        elif language == 'python':
            # For Python, find the version assignment and extract the version number
            match = re.search(r'version\s*=\s*[\'"](\d+\.\d+\.\d+)[\'"]', response.text)
            return match.group(1) if match else None
    else:
        logging.error(f"Failed to fetch the version file: {response.text}")
        return None

def update_version_file(branch, new_version, language):
    project_id = os.getenv('CI_PROJECT_ID')
    private_token = os.getenv('OUR_GITLAB_TOKEN')
    file_path_map = {
        'go': 'main.go',
        'javascript': 'package.json',
        'python': 'setup.py'  # Assuming setup.py for Python projects
    }
    file_path = file_path_map.get(language, '')
    
    if not file_path:
        logging.error("Unsupported language for version update.")
        return
    
    # Fetch the current file content
    get_url = f"https://gitlab.com/api/v4/projects/{project_id}/repository/files/{file_path}/raw?ref={branch}"
    headers = {'PRIVATE-TOKEN': private_token}
    response = requests.get(get_url, headers=headers)
    
    if not response.ok:
        logging.error(f"Failed to fetch the version file: {response.text}")
        return
    
    # Update the version in the file content
    if language == 'javascript':
        content = response.json()
        content['version'] = new_version
        updated_content = json.dumps(content)
    elif language == 'go':
        # Regex to find and replace the version line in Go file
        updated_content = re.sub(r'(@version\s+)(\d+\.\d+\.\d+)', f'\\g<1>{new_version}', response.text)
    elif language == 'python':
        # For Python setup.py, assuming a simple version line; might need adjustment
        updated_content = re.sub(r'(version=)(["\']\d+\.\d+\.\d+["\'])', f'\\g<1>"{new_version}"', response.text)
    
    # Commit the updated file back to the repository
    commit_url = f"https://gitlab.com/api/v4/projects/{project_id}/repository/files/{file_path}"
    commit_data = {
        'branch': branch,
        'content': updated_content,
        'commit_message': f'Update version to {new_version}',
        'author_email': 'bot@example.com',
        'author_name': 'Version Update Bot'
    }
    commit_response = requests.put(commit_url, headers=headers, json=commit_data)
    if commit_response.ok:
        logging.info(f"Version file {file_path} updated successfully to {new_version}.")
    else:
        logging.error(f"Failed to update the version file: {commit_response.text}")

def update_version(current_version=None, action=None):
    if action == "major":
        major += 1
        minor = 0
        patch = 0
    elif action == "minor":
        minor += 1
        patch = 0
    elif action == "patch":
        patch += 1
    return f"{major}.{minor}.{patch}"

def project_language():
    if os.path.exists('main.go'):
        return 'go'
    elif os.path.exists('package.json'):
        return 'javascript'
    elif os.path.exists('setup.py'):
        return 'python'
    else:
        return 'unknown'
        
def what_to_do(action, current_version):
    project_lang = project_language()
    #TODO create afuncton to get current version  that take branch and language
    if action[0] == "hotfix_to_staging":
        create_merge_request_to_develop()
        new_version = update_version(current_version, action[1])
        update_version_file("staging", new_version, project_lang)
    elif action[0] == "merge_staging_to_main":
        # current_version is from staging
        update_version_file("main", current_version, project_lang)
    elif action[0] == "merge_develop_to_staging":
        # current_version is from develop
        update_version_file("staging", current_version, project_lang)
    elif action[0] == "direct_merge_to_develop":
        new_version = update_version(current_version,action[1])
        update_version_file("develop", new_version, project_lang)

def main():
    logging.info("Automatic semantic versioning started...!")
    mr_id = os.getenv("CI_MERGE_REQUEST_IID")
    project_id = os.getenv('CI_PROJECT_ID')
    private_token = os.getenv('OUR_GITLAB_TOKEN')
    
    if mr_id and project_id and private_token:
        logging.info("Getting MR details.")
        mr_details = fetch_mr_details(mr_id)
        project_lang = project_language()
        current_version = get_current_version(project_id, private_token, project_lang)
        logging.info(f"Current version: {current_version}")
        
        action = identify_action(mr_details)
        logging.info(f"Identified action: {action}")
        if action and action != "skip":
            what_to_do(action, current_version)
        elif action == "skip":
            logging.info("Developer chose to skip; doing nothing.")
    else:
        logging.error("Missing required environment variables or MR ID.")


if __name__ == "__main__":
    main()