# Introduction

This document tries to consolidate maintenance practices and procedures for the CICD
pipeline.

The pipeline is designed to run unattended, however it relies on multiple moving parts
that can act up, either by becoming unavailable or by behaving unpredictably.

Jobs that are critical for the pipeline, i.e. which shouldn't in no circumstance fail
will alert it's failing status in the [server-alerts slack channel](https://app.slack.com/client/TMN1B5C69/C0475E5BRBK).

# Feature Environment

The feature environment is by far the part of the pipeline that has the most
infrastructural dependencies. It relies on:

- Successful DMS builds
- Permission from the user executing the nunet/device-management-service> pipeline to
execute pipelines in nunet/test-suite>
- LXD host with available resources
- Nebula network with available IPs
- Available CI server, currently parked at ci.nunet.io
- Testmo platform

The following are a collection of common problems, how to investigate them and possible
mitigation steps.

## Job `upload_testmo_report` fails

This job can fail for a multitude of reasons. It relies not only on test artifacts
produced by test stages in the feature environment, but also needs `upload_allure_report`
so that it know what's the URL for the allure report published results to add to the test
results.

Therefore, this job can fail when tests aren't executed, i.e. they don't produce the
necessary artifacts and/or the pipeline is unable to communicate with the ci server.

### Pipeline didn't produce the test artifacts

This case can be attested by looking at the test jobs that precede the
`upload_testmo_report` job.

Take a look at this [functional_tests job](https://gitlab.com/nunet/test-suite/-/jobs/8931440161).

It failed because the pipeline was unable to authenticate with the target virtual machines
so it could issue commands over ssh. This happens because there are other virtual machines
already parked at the nebula IPs.

Since the pipeline generates a new ssh private key for each execution, it will try to
connect to these old machines and the authentication will fail. This is intended behavior,
otherwise we would't know that the pipeline was using older virtual machines with possibly
a different version of DMS deployed to it.

To mitigate this, you need to first identify which repo triggered the downstream pipeline.
If it came from DMS, currently these virtual machines are deployed at `test.nunet.io`.
These pipelines can also come from the test-suite test pipeline. In this case they are
deployed to Sam's machine.

To cleanup the leftover VMs, log into the machine affected and run the following command:

```bash
lxc ls --format json | jq -r '.[].name' | grep feat-env | xargs -n 1 lxc rm --force
```

This will list all the LXD virtual machines, filter them by `feat-env` prefix and remove
them.

This will free the nebula IPs allocated to these virtual machines.
