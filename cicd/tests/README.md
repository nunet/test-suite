# Introduction

This part of the test-suite pipeline is a self-assessing pipeline configured to isolate
each test case into its separate child pipeline.

# Adding new tests

- add a new job entry in `cicd/tests/index.gitlab-ci.yml` following the pattern of the
previous jobs
- add a new child pipeline configuration in `cicd/tests/Your-Test.gitlab-ci.yml`
- in the child pipeline configuration you must import from
`cicd/tests/common-configs.gitlab-ci.yml` which configures the available stages
- add the necessary jobs to test in the child pipeline configuration preferrably by
including the part of the pipeline to be tested and doing the required adjustments for the
rules and the dependencies of the jobs imported

