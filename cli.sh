#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

usage() {
    echo "Usage: $0 <context> <action> [<subcommand>, ...] [additional arguments...]"
    echo
    echo "Available actions:"
    echo "  lxd create: Create LXD infrastructure"
    echo "  lxd destroy: Destroy LXD infrastructure"
    echo "  run <test-type> standalone: Run standalone functional tests"
    echo "  run <test-type> distributed: Run distributed functional tests"
    echo "  run <test-type> list: lists all available functional tests"
    echo "  validate feature-files: uses 'behave --dry-run' to validate gherkin feature files"
    echo "  serve allure-report: builds and serve using nginx the allure reports for the feature environment"
    echo "Any additional arguments will be forwarded to the executed script"
    exit 1
}

handle-malformed-cli-invocation() {
    local context=${1:-}
    local action=${2:-}
    
    if [[ -z $context ]]; then
        echo Context is empty
        usage
    elif [[ -z $action ]]; then
        echo Action is empty
        usage
    else
        echo "Error: Invalid action '$action' for context '$context'"
        usage
    fi
    exit 1
}

context="${1:-}"
action="${2:-}"

# Map context and action to script path
script_to_execute=()
case "$context" in
"lxd")
    case "$action" in
    "init")
        script_to_execute=(lxd init --auto --network-address=127.0.0.1 --network-port=8443 --storage-backend=dir)
        ;;
    "create")
        script_to_execute=("$SCRIPT_DIR/infrastructure/dms-on-lxd/make.sh")
        ;;
    "destroy")
        script_to_execute=("$SCRIPT_DIR/infrastructure/dms-on-lxd/destroy.sh")
        ;;
    *)
        handle-malformed-cli-invocation $context $action
        ;;
    esac
    ;;
"run")
    stage="$action"
    action="${3:-}"

    if ! [[ -d "$SCRIPT_DIR/stages/$stage" ]]; then
        echo "Stage $stage unavailable!"
        echo "available stages:"
        /bin/ls -1 "$SCRIPT_DIR"/stages -I README.md
        exit 1
    fi

    case "$action" in
    "standalone")
        script_to_execute=("$SCRIPT_DIR/lib/bash/feature_environment/run-standalone-tests.sh" "$stage")
        ;;
    "distributed")
        script_to_execute=("$SCRIPT_DIR/lib/bash/feature_environment/run-distributed-tests.sh" "$stage")
        ;;
    "list")
        script_to_execute=("$SCRIPT_DIR/lib/bash/feature_environment/list.sh" "$stage")
        "${script_to_execute[@]}"
        exit 0
        ;;
    *)
        handle-malformed-cli-invocation $context $action
        ;;
    esac
    ;;
"validate")
    case "$action" in
    "feature-files")
        script_to_execute=("$SCRIPT_DIR/lib/bash/feature_environment/validate-feature-files.sh")
        ;;
    *)
        handle-malformed-cli-invocation $context $action
        ;;
    esac
    ;;
"serve")
    case "$action" in
    "allure-report")
        script_to_execute=("$SCRIPT_DIR/lib/bash/feature_environment/build-and-serve-allure-reports.sh")
        ;;
    *)
        handle-malformed-cli-invocation $context $action
        ;;
    esac
    ;;
*)
    handle-malformed-cli-invocation $context
    ;;
esac

# Forward all arguments after the first three
script_to_execute+=("${@:4}")
echo "executing ${script_to_execute[@]}..."
"${script_to_execute[@]}"
