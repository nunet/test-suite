# 3. ssh for running remote commands

Date: 2024-06-21

## Status

Accepted

Expands [2. Feature environment](0002-feature-environment.md)

## Context

The ADR that this document expands the context for the architecture of the feature environment. At that time we decided to go for Gitlab CI, LXD and Nebula.
Although none of those technologies have been replaced, their actual use wasn't clear in the first document. This document aims to expand on that.

## Decision

We use Gitlab CI to run jobs interacting with LXD hosts that expose virtual machines capabilities through the LXD API. The jobs themselves must run in a runner
that is connected to the nebula network. The virtual machines spawned in the remote LXD hosts all join the nebula network.

We decide to replace `lxc exec` with `ssh` for remote code execution. `lxd exec` would simplify the setup, but it would make any solution coupled with that interface
less portable. By replacing `lxc exec` with `ssh` for the functional tests, we make it more likely to be able to execute these feature tests, once we add community
machines to the pool of available DMS nodes, as it's more likely that they would support ssh and not lxd.

## Consequences

While the automation process for spawning lxd virtual machines become a little more involved, using SSH will increase the portability of solutions created on top
of the architecture of provided by the feature environment as described in these ADRs.

