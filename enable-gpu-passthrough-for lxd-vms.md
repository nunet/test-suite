**These are the procedures I followed to pass GPU's to LXD virtual machines.**

**Intall LXD**

```
nunet@nunetapac01:~$ sudo snap install lxd
[sudo] password for nunet: 
lxd 5.20-51a2393 from Canonical✓ installed
```

**Init the LXD environment**

```
nunet@nunetapac01:~$ lxd init
Would you like to use LXD clustering? (yes/no) [default=no]: 
Do you want to configure a new storage pool? (yes/no) [default=yes]: 
Name of the new storage pool [default=default]: 
Name of the storage backend to use (dir, lvm, zfs, btrfs, ceph) [default=zfs]: 
Create a new ZFS pool? (yes/no) [default=yes]: 
Would you like to use an existing empty block device (e.g. a disk or partition)? (yes/no) [default=no]: 
Size in GiB of the new loop device (1GiB minimum) [default=30GiB]: 200 
Would you like to connect to a MAAS server? (yes/no) [default=no]: 
Would you like to create a new local network bridge? (yes/no) [default=yes]: 
What should the new bridge be called? [default=lxdbr0]: 
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: none
Would you like the LXD server to be available over the network? (yes/no) [default=no]: yes
Address to bind LXD to (not including port) [default=all]: all  
Port to bind LXD to [default=8443]: 
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]: 
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]: yes
config:
  core.https_address: '[::]:8443'
networks:
- config:
    ipv4.address: auto
    ipv6.address: none
  description: ""
  name: lxdbr0
  type: ""
  project: default
storage_pools:
- config:
    size: 200GiB
  description: ""
  name: default
  driver: zfs
profiles:
- config: {}
  description: ""
  devices:
    eth0:
      name: eth0
      network: lxdbr0
      type: nic
    root:
      path: /
      pool: default
      type: disk
  name: default
projects: []
cluster: null
```

**Launch a new LXD VM**

```
nunet@nunetapac01:~$ lxc launch ubuntu:22.04 sams-test-lxd
Creating sams-test-lxd
Starting sams-test-lxd                        
```

**Check GPU Hardware Availalblity in VM **

```
nunet@nunetapac01:~$ lxc exec sams-test-lxd bash
root@sams-test-lxd:~#

nunet@nunetapac01:~$ lspci -nn | grep -i nvidia
04:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
81:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
82:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
```

**Check NVIDIA Driver is loaded in VM / install it**

```
nunet@nunetapac01:~$ lxc exec sams-test-lxd bash
root@sams-test-lxd:~# nvidia-smi
NVIDIA-SMI has failed because it couldn't communicate with the NVIDIA driver. Make sure that the latest NVIDIA driver is installed and running.

root@sams-test-lxd:~# apt install nvidia-535-server
```

**check Driver is working**


```
nunet@nunetapac01:~$ lxc exec sams-test-lxd bash
root@sams-test-lxd:~# nvidia-smi
NVIDIA-SMI has failed because it couldn't communicate with the NVIDIA driver. Make sure that the latest NVIDIA driver is installed and running.
```

**I tried to blacklist the driver from the host os**
(This didnt work)

```
nunet@nunetapac01:~$ echo "blacklist nouveau" | sudo tee -a /etc/modprobe.d/blacklist.conf
sudo update-initramfs -u
reboot
```

```
nunet@nunetapac01:~$ nvidia-smi
Thu Feb  8 07:36:06 2024       
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 535.154.05             Driver Version: 535.154.05   CUDA Version: 12.2     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                 Persistence-M | Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  Tesla P100-PCIE-16GB           Off | 00000000:04:00.0 Off |                    0 |
| N/A   32C    P0              24W / 250W |    163MiB / 16384MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
|   1  Tesla P100-PCIE-16GB           Off | 00000000:81:00.0 Off |                    0 |
| N/A   37C    P0              25W / 250W |      4MiB / 16384MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
|   2  Tesla P100-PCIE-16GB           Off | 00000000:82:00.0 Off |                    0 |
| N/A   36C    P0              25W / 250W |      4MiB / 16384MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
                                                                                         
+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
|    0   N/A  N/A      3040      G   /usr/lib/xorg/Xorg                          119MiB |
|    0   N/A  N/A      3371      G   /usr/bin/gnome-shell                         43MiB |
|    1   N/A  N/A      3040      G   /usr/lib/xorg/Xorg                            4MiB |
|    2   N/A  N/A      3040      G   /usr/lib/xorg/Xorg                            4MiB |
+---------------------------------------------------------------------------------------+

nunet@nunetapac01:~$ lspci -nn | grep -i nvidia
04:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
81:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
82:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)

nunet@nunetapac01:~$ lspci -nnk -d 10de:
04:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
	Subsystem: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:118f]
	Kernel driver in use: nvidia
	Kernel modules: nvidiafb, nouveau, nvidia_drm, nvidia
81:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
	Subsystem: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:118f]
	Kernel driver in use: nvidia
	Kernel modules: nvidiafb, nouveau, nvidia_drm, nvidia
82:00.0 3D controller [0302]: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:15f8] (rev a1)
	Subsystem: NVIDIA Corporation GP100GL [Tesla P100 PCIe 16GB] [10de:118f]
	Kernel driver in use: nvidia
	Kernel modules: nvidiafb, nouveau, nvidia_drm, nvidia
```

**Stop the LXD VM , assign a GPU to it and restart **

```
nunet@nunetapac01:~$ lxc stop sams-test-lxd

nunet@nunetapac01:~$ lxc config device add sams-test-lxd gpu0 gpu pci=0000:04:00.0
Device gpu0 added to sams-test-lxd

nunet@nunetapac01:~$ lxc start sams-test-lxd
```

**Check NVIDA Driver Is Loaded in VM**

```
nunet@nunetapac01:~$ lxc exec sams-test-lxd -- nvidia-smi
Thu Feb  8 12:03:12 2024       
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 535.154.05             Driver Version: 535.154.05   CUDA Version: 12.2     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                 Persistence-M | Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  Tesla P100-PCIE-16GB           Off | 00000000:04:00.0 Off |                    0 |
| N/A   32C    P0              24W / 250W |    193MiB / 16384MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
                                                                                         
+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
+---------------------------------------------------------------------------------------+
```

Conclusion , it seems to work ok.  Need to work on how to integrate this into the pipeline so the runner can do this.
