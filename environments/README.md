# environments

Contain all files and scripts that are needed in order to spawn each environment that is used in testing stages (also to start the production environment -- which is the mainnet!)

## Summary

Similarly to other decentralized computing projects (as blockchains), the network is running on the hardware provisioned via independent devices. In NuNet case, there is an
additional complexity due to the fact that test networks have to resemble heterogeneity of the population of devices, operating systems and setups. Therefore, large portion
of the [NuNet CI/CD pipeline](NuNet-CI-pipeline) have to run not on centralized servers (e.g. in our case, via gitlab-ci runners), but on the geographically dispersed network.
In order to manage the full life-cycle of the platform, including testing of separate features and iterations of the network components, NuNet is using isolated *channels*
categorized into four *environments*:

* *feature* environment is used to run ci/cd pipeline on each merge request from individual developers' feature branches;
* *development* environment runs the ci/cd pipeline on the `develop` branch of the NuNet repositories;
* *staging* environment runs extensive pre-release testing on the frozen features in the staging branch;
* *production* environment runs the final releases of NuNet network, exposed to end users;
