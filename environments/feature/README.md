# Feature environment

## Purpose and audience

Branch: As per NuNet GIT Workflow, all individual development will happen on the feature branches created from develop branch and related to each issue assigned via the team process and development board.
When the developed feature is requested to merge to the develop branch, selected stages of the CI/CD pipeline are triggered on merge request (so that reviewers and approvers if it complies to relevant acceptance criteria). The Feature environment is used to run these stages.

## Architecture

Most of the stages require only static environment (i.e. can be executed via gitlab-runners on NuNet servers), but some of them may require distributed deployment of the platform components.
Note also, that the feature network may branch in a number of channels (see compute provider onboarding for details) for testing specific features or use-cases which need sourcing specific hardware.

## CI/CD stages

Feature environment is used to run the following CI/CD pipeline stages on merge request and on merge:

* static analysis;
* unit tests;
* static security tests;
* build (if needed by the feature);
* functional tests / API tests (if needed by the feature);

## Triggering schedule
Since merge requests are be frequent, Feature environment mostly runs on GitLab servers triggered by CI/CD pipeline stages. However, it is also augmented by a small network of NuNet team owned hardware (still geographically distributed in private offices), which can be automatically updated and triggered in a centralized manner via CI/CD pipeline stages.

When necessary, a developer can request and run tests in NuNet's virtual machines and containers hosted in NuNet cloud servers or can run preliminary tests on their local machines before pushing into GitLab (always prefered). This environment is dotted in the above diagram since it is not exactly defined in the NuNet infrastructure.