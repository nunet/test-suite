# Production environment

*Note: Transferred from old Wiki [page](https://gitlab.com/nunet/documentation/-/wikis/NuNet-test-process-and-environments); to be updated as per new developments.* 

## Purpose and public

This is the live environment used by the community to onboard machines/devices or to use the computational resources available on the NuNet platform.

## CI/CD pipeline

No CI/CD pipeline stages are running on production environment. However, all users are provided with tools and are encouraged to report any bugs or file feature requests following the [https://gitlab.com/nunet/documentation/-/wikis/Contribution-Guidelines].

## Architecture

The Production environment contains all community machines/devices connected to production network.

## Production Release

When the tests in the Testnet (staging environment) are finished with success and approved by the testers, the module(s)/API(s) should be released to production. The following processes are being defined:

* versioning process: versioning of modules and APIs;
* compatibility/deprecation process: releasing modules/APIs that do not have compatibility with others modules/APIs currently running on the platform should be avoided since NuNet is a highly decentralized network; however old versions should be deprecated so maintaining the compatibility will not create other problems related to security, performance, code readability, etc.
* communication process: how the community is notified of modules updates, bugs, security issues
* updating process: how the modules/APIs are updated.