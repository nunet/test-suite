local num_of_machines = std.parseInt(std.extVar("NUM_OF_MACHINES"));

local runTests = function(index) 
{
    stage: "test",
    allow_failure: true,
    image: {
      name: "ubuntu:jammy"
    },
    tags: ["lxd"],
    before_script: [
      "apt update",
      "apt install python3 python3-pip curl wget git-lfs python3-venv expect -y"
    ],
    script: |||
      python3 -m venv .venv
      source .venv/bin/activate
      pip install -r requirements.txt
      if ! command -v nunet &> /dev/null
      then
        apt install -f ./dist/nunet-dms_*.deb -y
      fi
      behave --junit stages/functional_tests/features/device-management-service/cli-tests/
    |||,
    artifacts: {
      paths: ["reports/"],
      name: "behave-test-results-" + std.toString(index),
      when: "always"
    },
    needs: [{
        project: "nunet/device-management-service",
        job: "Build",
        ref: "$PARENT_PIPELINE_BRANCH",
        artifacts: true
    }]
  };


local generateUploadStage = function(index) {

  
    stage: ".post",
    needs: ["Test Run " + std.toString(index)],
    dependencies: ["Test Run " + std.toString(index)],
    image: {
      name: "node:lts"
    },
    before_script: [
      "npm install -g @testmo/testmo-cli"
    ],
    script: [
      "ls -lha reports",
      'testmo automation:run:submit --instance https://nunet.testmo.net --project-id 2 --name "BDD Test run from Machine ' + std.toString(index) + '" --source "frontend" --results reports/*.xml'
    ]
  
};

{
    ["Test Run " + index]: runTests(index)
    for index in std.range(1, num_of_machines)
}

{
    ["Upload Results from Test " + index]: generateUploadStage(index)
    for index in std.range(1, num_of_machines)
}