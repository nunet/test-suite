#!/usr/bin/env bash
set -euo pipefail

if ! terraform apply -auto-approve plan.out; then
    if [ -v TF_APPLY_STRICT ]; then
        echo something went wrong applying terraform. Reverting...
        bash destroy.sh
        exit 1
    else
        echo something went wrong applying terraform but it wont revert
        exit 2
    fi
else
    echo terraform successfully applied
fi

