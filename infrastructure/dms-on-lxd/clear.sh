#!/usr/bin/env bash

set -euo pipefail

echo this script does destructive operations to this folder and the LXD host
if [[ "${1:-no_confirm}" != "confirm" ]]; then
    echo 'please rerun this script again with confirming the operation:'
    echo \$ $0 confirm
    exit 1
fi

echo removing LXD instances and trust tokens...
lxc ls --format=json | jq -r '.[].name' | xargs -n1 lxc rm --force
lxc config trust list --format json | jq -r '.[].fingerprint' | xargs -n1 lxc config trust rm

echo removing state, cache and configs
sudo rm -rf config-*.yml .*-token lxc-certs/ cache/ terraform.tfstate* .terraform
