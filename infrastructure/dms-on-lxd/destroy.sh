#!/usr/bin/env bash
# Cleanup script for DMS on LXD infrastructure
#
# Execution flow:
# 1. Sources lib.sh for common functions
# 2. Handles Docker runner if enabled via DMS_ON_LXD_DOCKER
# 3. Initializes terraform
# 4. Generates config based on DMS_ON_LXD_ENV
# 5. Tests and adds LXD remote hosts
# 6. Destroys all terraform-managed infrastructure
#
# Dependencies:
# - lib.sh: Common functions
# Side effects:
# - Removes all LXD instances created by terraform
# - Cleans up terraform state
# - Keeps configuration files and SSH keys intact

set -euo pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $SCRIPT_DIR/lib.sh

handle-docker-runner destroy.sh

terraform init

generate-config
test-and-add-remote-lxd-hosts

terraform apply -destroy -auto-approve

