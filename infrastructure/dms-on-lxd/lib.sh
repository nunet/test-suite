#!/usr/bin/env bash

# Directory containing this script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source "$SCRIPT_DIR/check.sh"

# Handles running commands through Docker if enabled
# Args:
#   $1: Filename to execute in Docker container
handle-docker-runner() {
    file_to_execute="${1:?you must pass the filename to be executed in docker}"
    if [[ "${DMS_ON_LXD_APPLY_DOCKER_PATCH:-true}" == "true" ]]; then
        echo applying docker patch...
        sudo iptables -F DOCKER-USER || true
        sudo iptables -I DOCKER-USER -j RETURN
        sudo iptables -I DOCKER-USER -o lxdbr0 -j ACCEPT
        sudo iptables -I DOCKER-USER -i lxdbr0 -j ACCEPT
    else
        echo skip applying docker patch...
    fi
    if ! [[ -f /.dockerenv ]] && [[ "${DMS_ON_LXD_DOCKER:-true}" == "true" ]]; then
        if command -v snap >/dev/null 2>&1 && snap list | grep --quiet lxd; then
            mkdir -p $HOME/snap/lxd/common/config
            lxd_certs_dir=$HOME/snap/lxd/common/config
        else
            mkdir -p $HOME/.config/lxc
            lxd_certs_dir=$HOME/.config/lxc
        fi
        echo building docker image...
        docker build --build-arg NEW_UID=$(id -u) -t dms-on-lxd $SCRIPT_DIR
        docker_run_cmd=(
            docker run -it --rm
            -v $SCRIPT_DIR:/app
            -v "$DMS_DEB_FILE:/opt/dms.deb"
            -v "$lxd_certs_dir:/home/newuser/.config/lxc"
            --user "$(id -u):$(getent group lxd | cut -d: -f3)"
            --network=host
        )
        if [[ -S /var/lib/lxd/unix.socket ]]; then
            docker_run_cmd+=(-v /var/lib/lxd/unix.socket:/var/lib/lxd/unix.socket)
        elif [[ -S /var/snap/lxd/common/lxd/unix.socket ]]; then
            docker_run_cmd+=(-v /var/snap/lxd/common/lxd/unix.socket:/var/snap/lxd/common/lxd/unix.socket)
        else
            echo lxd unix socket not found. Is lxd running?
            exit 1
        fi
        export DMS_DEB_FILE=/opt/dms.deb  # this is important in case the developer changes the debian file in the host system, otherwise terraform breaks
        export DMS_ON_LXD_APPLY_DOCKER_PATCH=false
        for dms_env in $(env | grep DMS); do
            docker_run_cmd+=(--env $dms_env)
        done
        docker_run_cmd+=(dms-on-lxd bash $file_to_execute "${@:2}")
        if [[ "${DMS_ON_LXD_VERBOSE:-false}" == "true" ]]; then
            echo "${docker_run_cmd[@]}"
        fi
        "${docker_run_cmd[@]}"
        exit 0
    fi
}

# Tests LXD hosts for reachability and adds them as remotes
# Creates reachable_hosts.yml and unreachable_hosts.yml files
# Reads hosts from config.yml or config-$DMS_ON_LXD_ENV.yml
test-and-add-remote-lxd-hosts() {
    echo 'lxd_hosts:' > unreachable_hosts.yml
    echo 'lxd_hosts:' > reachable_hosts.yml

    echo testing hosts for reachability...

    if [[ -v DMS_ON_LXD_ENV ]]; then
        CONFIG_FILE=config-$DMS_ON_LXD_ENV.yml
    else
        CONFIG_FILE=config.yml
    fi

    echo using config file: $CONFIG_FILE

    for host in $(yq -rc '.lxd_hosts[]' "$CONFIG_FILE"); do
        host_url=$(echo $host | jq -rc '"https://" + .host + ":" + ((.port // 8443)|tostring)')
        if curl --silent $host_url --insecure --max-time 10; then
            echo $host_url is reachable
            echo "- $host" >> reachable_hosts.yml
            lxd_hostname=$(echo $host | jq -rc '.host')
            {
                if [[ "$host" =~ "token" ]]; then
                    lxc remote add --accept-certificate --token "$(echo $host | jq -rc '.token')" "$lxd_hostname" "$host_url"
                elif [[ "$host" =~ "password" ]]; then
                    lxc remote add --accept-certificate --password "$(echo $host | jq -rc '.password')" "$lxd_hostname" "$host_url"
                fi
            } || true 
        else
            echo $host_url is NOT reachable
            echo "- $(echo $host | jq -rc '{host: .host, port: .port}')" >> unreachable_hosts.yml
        fi
    done
}

# Generates configuration files and sets up terraform workspace
# Uses environment variables:
#   DMS_ON_LXD_ENV: Environment name for config files
#   DMS_DEB_FILE: Path to DMS debian package
#   INSTANCE_TYPE: LXD instance type (default: virtual-machine) 
#   DISTRO_VERSION: OS version (default: 24.04)
#   DISTRO_NAME: OS distribution (default: ubuntu)
#   LXD_LOCAL_PORT: Local LXD API port (default: 8443)
generate-config() {
    echo generating config...
    if [[ -v DMS_ON_LXD_ENV ]]; then
        export TF_VAR_env_name=$DMS_ON_LXD_ENV
        CONFIG_FILE_NAME=config-$DMS_ON_LXD_ENV.yml
        TOKEN_FILE=.$DMS_ON_LXD_ENV-token

        if ! [[ -f "${DMS_DEB_FILE:?please provide the dms debian filepath}" ]]; then
            echo file not found: "$DMS_DEB_FILE"
            exit 1
        fi
        if ! [[ -f .$DMS_ON_LXD_ENV-token ]]; then
            lxc config trust add --name "dms-on-lxd-$DMS_ON_LXD_ENV" | tail -n 1 > ".$DMS_ON_LXD_ENV-token"
        fi

        ENV_TOKEN=$(cat $TOKEN_FILE)
        PUB_KEY=$(cat lxd-key.pub)
        cat > "$CONFIG_FILE_NAME" <<EOL
dms_instances_count: 2
dms_deb_filepath: "/opt/dms.deb"

timezone: "Etc/UTC"
ssh_pub_key: "$PUB_KEY"
instance_type: "${INSTANCE_TYPE:-virtual-machine}"
distro_version: "${DISTRO_VERSION:-24.04}"
distro_name: "${DISTRO_NAME:-ubuntu}"
instance_name_prefix: "$DMS_ON_LXD_ENV-"
enable_nebula: false

limits:
  memory: "${LXD_LIMIT_MEM:-4GB}"
  cpu: ${LXD_LIMIT_CPU:-2}

lxd_hosts:
- host: localhost
  port: ${LXD_LOCAL_PORT:-8443}
  token: $ENV_TOKEN
EOL

        if ! terraform workspace list | grep $DMS_ON_LXD_ENV; then
            terraform workspace new $DMS_ON_LXD_ENV
        fi
        terraform workspace select $DMS_ON_LXD_ENV
    fi

    if [[ -v GITLAB_CI ]]; then

        PUB_KEY=$(cat lxd-key.pub)
        DMS_DEB_FILE=$(ls -1 dist/nunet-dms*amd64.deb)

        cat >> config.yml <<EOL
ssh_pub_key: "$PUB_KEY"
instance_type: "${INSTANCE_TYPE:-virtual-machine}"
dms_deb_filepath: "$DMS_DEB_FILE"
distro_version: "${DISTRO_VERSION:-24.04}"
distro_name: "${DISTRO_NAME:-ubuntu}"
instance_name_prefix: "feat-env-$CI_PIPELINE_ID-"
enable_nebula: true
EOL
    fi

    if command -v snap >/dev/null 2>&1 && snap list | grep --quiet lxd; then
        echo identified lxd has been installed via snap. Configuring environment accordingly...
        export TF_VAR_lxd_install_source=snap
    else
        echo identified lxd has been installed by means other than snap. Configuring environment accordingly...
        export TF_VAR_lxd_install_source=other
    fi
}

