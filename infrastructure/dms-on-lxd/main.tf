locals {
  default_config = {
    timezone       = "Etc/UTC"
    ssh_pub_key    = file("lxd-key.pub")
    instance_type  = "virtual-machine"
    distro_name    = "ubuntu"
    distro_version = "24.04"
    limits = {
      memory = "4GB"
      cpu    = 2
    }
    enable_nebula        = false
    instance_name_prefix = ""
    dms_deb_filepath     = "/opt/dms.deb"
    vm_lifetime_minutes  = 60
  }
  file_suffix = (
    var.env_name == null
    ? ""
    : "-${var.env_name}"
  )
  yaml_config     = yamldecode(file("config${local.file_suffix}.yml"))
  config_file     = merge(local.default_config, local.yaml_config)
  reachable_hosts = yamldecode(file("reachable_hosts.yml"))

  ssh_pub_key = local.config_file.ssh_pub_key
  lxd_hosts   = local.reachable_hosts.lxd_hosts
  lxd_config_dir = pathexpand(
    var.lxd_install_source == "snap"
    ? "~/snap/lxd/common/config"
    : "~/.config/lxc"
  )

  hosts_length = length(local.lxd_hosts)
  nebula_users = (
    local.config_file.enable_nebula == true
    ? local.config_file.nebula_users
    : []
  )

  # if it's not specified the number of dms instances, default to deploying one instance per host
  dms_instances_count = lookup(local.config_file, "dms_instances_count", local.hosts_length)
  dms_instances = {
    for i in range(local.dms_instances_count) :
    "dms-${i}" =>
    {
      host = local.lxd_hosts[i % local.hosts_length].host
      nebula_user = (
        local.config_file.enable_nebula == true
        ? local.nebula_users[i]
        : null
      )
    }
  }
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true
  config_dir                   = local.lxd_config_dir
  dynamic "remote" {
    for_each = [for host in local.lxd_hosts : host if contains(keys(host), "password")]
    content {
      name     = remote.value.host
      address  = "https://${remote.value.host}:${lookup(remote.value, "port", 8443)}"
      password = remote.value.password
    }
  }

  dynamic "remote" {
    for_each = [for host in local.lxd_hosts : host if contains(keys(host), "token")]
    content {
      name    = remote.value.host
      address = "https://${remote.value.host}:${lookup(remote.value, "port", 8443)}"
      token   = remote.value.token
    }
  }
}

resource "lxd_instance" "dms_on_lxd" {
  for_each = local.dms_instances

  name      = "${local.config_file.instance_name_prefix}${each.key}"
  image     = "${local.config_file.distro_name}:${local.config_file.distro_version}"
  profiles  = ["default"]
  ephemeral = true
  remote    = each.value.host
  type      = local.config_file.instance_type
  running   = true

  config = {
    "boot.autostart" = true
    "user.user-data" = <<EOF
#cloud-config
disable_root: 0
ssh_authorized_keys:
  - ${local.ssh_pub_key}
package_upgrade: true
packages:
  - git
  - kitty-terminfo
  - python3-pip
  - python3-behave
timezone: ${local.config_file.timezone}
runcmd:
  - shutdown +${local.config_file.vm_lifetime_minutes}
  - |
    while ! [ -f /opt/init.sh ]; do
        echo waiting for /opt/init.sh to be provisioned...
        sleep 5
    done
  - bash /opt/init.sh
EOF
  }

  limits = {
    for resource, constraint in local.config_file.limits :
    resource => constraint
  }
}

resource "lxd_instance_file" "dms_deb" {
  for_each = local.dms_instances

  instance    = lxd_instance.dms_on_lxd[each.key].name
  remote      = lxd_instance.dms_on_lxd[each.key].remote
  source_path = local.config_file.dms_deb_filepath
  target_path = "/opt/dms.deb"

  lifecycle {
    ignore_changes = [mode]
  }
}

resource "lxd_instance_file" "init" {
  for_each   = local.dms_instances
  depends_on = [lxd_instance_file.dms_deb]

  instance = lxd_instance.dms_on_lxd[each.key].name
  remote   = lxd_instance.dms_on_lxd[each.key].remote
  content = sensitive(templatefile(
    "${path.root}/init.sh.tpl",
    {
      enable_nebula = local.config_file.enable_nebula
      nebula_user   = each.value.nebula_user
    }
  ))
  target_path = "/opt/init.sh"

  lifecycle {
    ignore_changes = [mode]
  }
}

resource "lxd_instance_file" "wait_for_dms_script" {
  for_each   = local.dms_instances
  depends_on = [lxd_instance_file.dms_deb]

  instance    = lxd_instance.dms_on_lxd[each.key].name
  remote      = lxd_instance.dms_on_lxd[each.key].remote
  source_path = "${path.root}/wait-for-dms.sh"
  target_path = "/opt/wait-for-dms.sh"

  lifecycle {
    ignore_changes = [mode]
  }
}

resource "local_file" "ipv4_list" {
  content = (
    local.config_file.enable_nebula == true
    ? join("\n", [for dms_instance in local.dms_instances : dms_instance.nebula_user.ipv4])
    : join("\n", [for dms_instance in values(lxd_instance.dms_on_lxd) : [for ip in dms_instance.interfaces.eth0.ips : ip.address if ip.family == "inet"][0]])
  )
  filename = "vms-ipv4-list${local.file_suffix}.txt"
}

resource "local_file" "os_release" {
  content  = <<EOF
DISTRO_NAME=${local.config_file.distro_name}
DISTRO_VERSION=${local.config_file.distro_version}
EOF
  filename = "os-release${local.file_suffix}.sh"
}
