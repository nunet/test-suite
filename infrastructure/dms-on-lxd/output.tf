locals {
  instances_data = flatten([for instance_name, instance_data in lxd_instance.dms_on_lxd.* : values(instance_data)])
}

output "lxd_vm_addresses" {
  value = [for instance_data in local.instances_data : "${instance_data.remote}:${instance_data.name}"]
}

output "lxd_install_source" {
  value = var.lxd_install_source
}

output "lxd_config_dir" {
  value = local.lxd_config_dir
}
