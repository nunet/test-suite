#!/usr/bin/env bash
set -euo pipefail

max_iter=${MAX_ITER:-30} # defaults to 5 minutes, polling every 10 second
polling_interval_seconds=${POLLING_INTERVAL_SECONDS:-10} # wait 20 seconds before testing nunet cmd again
iter=0
while ! lxc exec ${TARGET_INSTANCE:?target instance not set} which nunet; do
    echo dms installation still not ready
    iter=$(( iter+1 ))
    if [[ $iter -ge $max_iter ]]; then
        echo timeout waiting for dms to be ready
        exit 1
    fi

    if lxc exec ${TARGET_INSTANCE:?target instance not set} ls /opt/done >/dev/null 2>&1 && ! lxc exec ${TARGET_INSTANCE:?target instance not set} which nunet >/dev/null 2>&1; then
        echo it seems init.sh finished executing but nunet is still not ready
        echo something went wrong with dms installation
        exit 2
    fi

    sleep $polling_interval_seconds
done

echo dms is ready

