#!/bin/bash
set -euo pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PROJECT_DIR="${CI_PROJECT_DIR:-$(grep --only-matching --extended-regexp '.*test-suite' <<<$SCRIPT_DIR)}"

# setopt -s globstar
for TEST_ALLURE_DIR_RESULTS in $PROJECT_DIR/test-results/**/results; do
    TEST_ALLURE_DIR_REPORT="$PROJECT_DIR/allure-reports/$(basename $(dirname $TEST_ALLURE_DIR_RESULTS))"
    allure_gen_cmd=(allure generate --clean $TEST_ALLURE_DIR_RESULTS -o $TEST_ALLURE_DIR_REPORT)
    echo executing "${allure_gen_cmd[@]}"...
    if [[ -f /.dockerenv ]]; then
        ${allure_gen_cmd[@]}
    else
        echo delegating execution to docker using nunet-behave image...
        docker run -it --rm \
            --workdir=$PROJECT_DIR \
            -v $PROJECT_DIR:$PROJECT_DIR \
            --entrypoint="" \
            nunet-behave ${allure_gen_cmd[@]}
    fi
done

# will serve on port 8080 up to port 8999, the first that is free to bind
nginx_serve_port=$(comm -23 <(seq 8080 8999) <(ss -tan | awk '{print $4}' | cut -d':' -f2 | grep "[0-9]\{1,5\}$" | sort | uniq) | head -n 1)

if ! docker image ls | awk '{print $1}' | grep nunet-nginx-autoindex; then
    echo building nginx image...
    docker build --pull -t nunet-nginx-autoindex - <<EOF
FROM nginx
RUN sed -i -e '/location.*\/.*{/a autoindex on\;' /etc/nginx/conf.d/default.conf
EOF
fi

echo serving allure reports at http://localhost:$nginx_serve_port
echo CTRL+C to exit

docker run --rm \
    --name nunet-allure-reports \
    -v $PROJECT_DIR/allure-reports:/usr/share/nginx/html:ro \
    -p $nginx_serve_port:80 \
    nunet-nginx-autoindex >/dev/null 2>&1
