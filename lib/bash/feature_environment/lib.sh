#!/usr/bin/env bash

# This script sets up the environment and provides utility functions for running
# distributed and standalone tests
# It handles test result collection, Docker container execution, and environment configuration.

# Directory containing this script, resolved using BASH_SOURCE
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Base project directory - either from CI variable or derived from script location
export PROJECT_DIR="${CI_PROJECT_DIR:-$(grep --only-matching --extended-regexp '.*test-suite' <<<$SCRIPT_DIR)}"

# SSH key file used for accessing test VMs
export SSH_KEY_FILE=$PROJECT_DIR/infrastructure/dms-on-lxd/lxd-key

# Skip onboarding check for tests (typically set in CI/CD pipeline)
export BEHAVE_SKIP_ONBOARD_CHECK=true

if [[ -v DMS_ON_LXD_ENV ]]; then
    # Load OS release info for specific environment
    export OS_RELEASE_FILE="$PROJECT_DIR/infrastructure/dms-on-lxd/os-release-$DMS_ON_LXD_ENV.sh"
    export INVENTORY_FILE="$PROJECT_DIR/infrastructure/dms-on-lxd/vms-ipv4-list-$DMS_ON_LXD_ENV.txt"
else
    # Load default OS release info
    export OS_RELEASE_FILE="$PROJECT_DIR/infrastructure/dms-on-lxd/os-release.sh"
    export INVENTORY_FILE=$PROJECT_DIR/infrastructure/dms-on-lxd/vms-ipv4-list.txt
fi

# load os release info if available
if [[ -f $OS_RELEASE_FILE ]]; then
    source $OS_RELEASE_FILE
else
    echo "WARN: $OS_RELEASE_FILE doesn't exist"
fi

# Base directories for test results
#
export STANDALONE_TEST_JUNIT_DIR_BASE=$PROJECT_DIR/test-results/junit-reports/${stage}/standalone
export DISTRIBUTED_TEST_JUNIT_DIR_BASE=$PROJECT_DIR/test-results/junit-reports/${stage}/distributed
export STANDALONE_TEST_ALLURE_DIR_BASE="$PROJECT_DIR/test-results/${stage}_$DISTRO_NAME-$DISTRO_VERSION"_standalone
export DISTRIBUTED_TEST_ALLURE_DIR_BASE="$PROJECT_DIR/test-results/${stage}_$DISTRO_NAME-$DISTRO_VERSION"_distributed

# Constructs the behave command with appropriate parameters
# Args:
#   $1: Test type ('distributed' or 'standalone')
#   $2+: Additional arguments to pass to behave (optional)
get-behave-cmd() {
    local _stage="${1:?get-behave-cmd: First argument must be the stage}"
    ARG_CHECK_REGEX="^(distributed|standalone)$"
    if ! [[ $2 =~ $ARG_CHECK_REGEX ]]; then
        echo "get-behave-cmd: second argument must be one of 'distributed' or 'standalone'"
        exit 1
    else
        TEST_TYPE=$2
    fi
    BEHAVE_CMD=(
        behave
        -D inventory_file=$INVENTORY_FILE
        -D ssh_key_file=$SSH_KEY_FILE
        -D step_timeout=${BEHAVE_STEP_TIMEOUT:-30} # in seconds
        --tags="$TEST_TYPE"
        --no-skipped
    )
    
    # Add Allure reporting if enabled
    if [[ "${BEHAVE_ENABLE_ALLURE:-false}" == "true" ]]; then
        BEHAVE_CMD+=(-f allure_behave.formatter:AllureFormatter -o $TEST_ALLURE_DIR_RESULTS)
    fi
    
    # Add JUnit reporting if enabled
    if [[ "${BEHAVE_ENABLE_JUNIT:-false}" == "true" ]]; then
        BEHAVE_CMD+=(--junit --junit-directory $TEST_JUNIT_DIR)
    fi
    
    # Add feature files or specific test arguments
    if [[ -v 3 ]] && [[ "$3" != "" ]]; then
        BEHAVE_CMD+=("${@:3}")
    else
        BEHAVE_CMD+=(stages/"$_stage"/features/device-management-service)
    fi
}

# Handles execution of tests in Docker environment
# Args:
#   $1: Script file to execute in Docker
#   $2+: Additional arguments to pass to the script
handle-docker-runner() {
    file_to_execute="${1:?you must pass the filename to be executed in docker}"
    if ! [[ -f /.dockerenv ]] && [[ "${FEATURE_ENVIRONMENT_DOCKER:-true}" == "true" ]]; then
        echo building docker image...
        docker build --build-arg NEW_UID=$(id -u) -t nunet-behave $PROJECT_DIR/cicd/tools/behave
        
        # Construct Docker run command with appropriate mounts and environment
        docker_run_cmd=(
            docker run -it --rm
            -v $PROJECT_DIR:$PROJECT_DIR
            --workdir $PROJECT_DIR
            --user "$(id -u):$(getent group lxd | cut -d: -f3)"
            --network=host
        )
        
        # Forward all DMS-related environment variables
        for dms_env in $(env | grep -e 'DMS\|BEHAVE'); do
            docker_run_cmd+=(--env $dms_env)
        done
        
        # Mount DMS DEB file if specified
        if [[ -v DMS_DEB_FILE ]]; then
            docker_run_cmd+=(-v $DMS_DEB_FILE:$DMS_DEB_FILE)
        fi
        
        docker_run_cmd+=(nunet-behave bash $file_to_execute "${@:2}")
        echo "${docker_run_cmd[@]}"
        "${docker_run_cmd[@]}"
        exit 0
    fi
}

# Sets up directories for standalone test results
# Args:
#   $1: VM IP address
get-standalone-test-results-dir () {
    local usage="usage: get-standalone-test-results-dir vm-ip"
    local vm_ip="${1?:$usage}"
    local allure_report_relative_dir="${stage}_$DISTRO_NAME-$DISTRO_VERSION"_standalone_"$vm_ip"
    export TEST_JUNIT_DIR="$STANDALONE_TEST_JUNIT_DIR_BASE/$DISTRO_NAME-$DISTRO_VERSION/$vm_ip"
    export TEST_ARTIFACTS_DIR="$STANDALONE_TEST_ALLURE_DIR_BASE"_"$vm_ip"
    export TEST_ALLURE_DIR_RESULTS="$STANDALONE_TEST_ALLURE_DIR_BASE"_"$vm_ip"/results
    export TEST_ALLURE_DIR_REPORT="$STANDALONE_TEST_ALLURE_DIR_BASE"_"$vm_ip"/report

    # Set Allure reports URL if running in CI environment
    if [[ -v CI_REPORTS_FINAL_URI ]]; then
        export TEST_ARTIFACTS_URL="$CI_REPORTS_FINAL_URI/$allure_report_relative_dir"
        export ALLURE_REPORTS_URL="$CI_REPORTS_FINAL_URI/$allure_report_relative_dir"/report
    fi
}

# Sets up directories for distributed test results
get-distributed-test-results-dir () {
    local allure_report_relative_dir="${stage}_$DISTRO_NAME-$DISTRO_VERSION"_distributed

    export TEST_JUNIT_DIR="$DISTRIBUTED_TEST_JUNIT_DIR_BASE"/"$DISTRO_NAME-$DISTRO_VERSION"
    export TEST_ARTIFACTS_DIR="$PROJECT_DIR/test-results/$allure_report_relative_dir"
    export TEST_ALLURE_DIR_RESULTS="$PROJECT_DIR/test-results/$allure_report_relative_dir/results"
    export TEST_ALLURE_DIR_REPORT="$PROJECT_DIR/test-results/$allure_report_relative_dir"_report
    
    # Set Allure reports URL if running in CI environment
    if [[ -v CI_REPORTS_FINAL_URI ]]; then
        export TEST_ARTIFACTS_URL="$CI_REPORTS_FINAL_URI/$allure_report_relative_dir"
        export ALLURE_REPORTS_URL="$CI_REPORTS_FINAL_URI/$allure_report_relative_dir"_report
    fi
}
