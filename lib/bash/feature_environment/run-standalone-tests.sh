#!/usr/bin/env bash
set -euo pipefail

stage="${1:?Usage: $0 <stage>}"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source "$SCRIPT_DIR/lib.sh"

handle-docker-runner $SCRIPT_DIR/run-standalone-tests.sh "${@:1}"

close-ssh-tunnel() {
  tunnel_pid=$(lsof -i :9999 | awk '{print $2}' | tail +2)
  kill "$tunnel_pid"
}

for vm_ip in $(cat $INVENTORY_FILE); do
  get-standalone-test-results-dir "$vm_ip"
  # TMP_INVENTORY_FILE is a temporary file populated with a single ipv4 from the vm ips inventory
  # internally, in the tests, provisioning a single machine is a special case of provisioning multiple machines
  TMP_INVENTORY_FILE=$(mktemp)
  echo $vm_ip > $TMP_INVENTORY_FILE

  echo start standalone tests for $vm_ip
  echo testing ssh connection...
  ssh-keygen -R $vm_ip || true
  if ! ssh \
      -o IdentitiesOnly=yes \
      -o StrictHostKeyChecking=no \
      -i $PROJECT_DIR/infrastructure/dms-on-lxd/lxd-key \
      root@$vm_ip -- echo ssh connection OK; then
    echo ssh test failed!
    echo will continue with the tests anyway...
  fi
  echo setting up tunnel for DMS port 9999...
  nohup ssh \
    -4 -N -L 9999:localhost:9999 \
    -o IdentitiesOnly=yes \
    -o StrictHostKeyChecking=no \
    -i $PROJECT_DIR/infrastructure/dms-on-lxd/lxd-key \
    root@$vm_ip &
  sleep 10
  echo running device_api tests againts $vm_ip...

  get-behave-cmd $stage standalone "${@:2}"
  echo "${BEHAVE_CMD[@]}"
  if ! "${BEHAVE_CMD[@]}"; then
    touch failed_tests
  fi

  echo closing ssh tunnel...
  if ! close-ssh-tunnel; then
    echo something went wrong destroying the tunnel...
  fi

  echo getting hardware information from vm...
  mkdir -p $TEST_ARTIFACTS_DIR
  ssh -o ConnectTimeout=10 -o StrictHostKeyChecking=no -i $PROJECT_DIR/infrastructure/dms-on-lxd/lxd-key root@$vm_ip -- lshw -json > $TEST_ARTIFACTS_DIR/hardware.json
  ssh -o ConnectTimeout=10 -o StrictHostKeyChecking=no -i $PROJECT_DIR/infrastructure/dms-on-lxd/lxd-key root@$vm_ip -- lshw -html > $TEST_ARTIFACTS_DIR/hardware.html
  ssh -o ConnectTimeout=10 -o StrictHostKeyChecking=no -i $PROJECT_DIR/infrastructure/dms-on-lxd/lxd-key root@$vm_ip -- uname --machine > $TEST_ARTIFACTS_DIR/cpu_architecture.txt
  ssh -o ConnectTimeout=10 -o StrictHostKeyChecking=no -i $PROJECT_DIR/infrastructure/dms-on-lxd/lxd-key root@$vm_ip -- bash -c 'lspci | grep VGA' > $TEST_ARTIFACTS_DIR/pci_vga.txt
  echo $vm_ip > $TEST_ARTIFACTS_DIR/vm-ip.txt
  echo finish standalone tests for $vm_ip
done
