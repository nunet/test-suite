#!/bin/bash
set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
PROJECT_ROOT="${SCRIPT_DIR//test-suite*/'test-suite'}"

cd $PROJECT_ROOT

feature_files_list=$(find "./stages" -name "*.feature" -type f | grep -v '.disabled')
behave_format_cmd_base=(behave --quiet --dry-run --tags="~wip")

if [[ "${BEHAVE_VERBOSE_VALIDATION:-false}" == "false" ]]; then
    behave_format_cmd_base+=(--format rerun)
fi

execute_behave() {
    if [[ -f /.dockerenv ]]; then
        ${behave_format_cmd[@]}
    else
        docker run -it --rm \
            --entrypoint="" \
            --workdir=/app \
            -v $PROJECT_ROOT:/app \
            nunet-behave \
            ${behave_format_cmd[@]}
    fi
}

for feature_file in $feature_files_list; do
    echo "linting $feature_file..."
    behave_format_cmd=(${behave_format_cmd_base[@]} "$feature_file")

    if ! execute_behave; then
        echo "[ERROR] failed to lint $feature_file!"
        echo "$feature_file" >> /tmp/failed_feature_files_lint
        sleep 2
    fi
done

# this will execute the lint again this time against all the files and output only a brief
# summary of the steps, which is important to easily identify if there are undefined steps
echo ""
echo Summary:
for stage in $(find ./stages -name 'environment.py' | grep -v .disabled | xargs -n 1 dirname); do
    echo Stage $(basename $stage):
    
    behave_format_cmd=(${behave_format_cmd_base[@]} --format rerun $(find $stage -name "*.feature" | grep -v .disabled))
    execute_behave
done

if [[ -f /tmp/failed_feature_files_lint ]]; then
    echo "The following feature files failed to lint:"
    cat /tmp/failed_feature_files_lint
    echo ""
    echo "TIP: if any step appear as undefined, you need to add the @wip tag to it"
    rm /tmp/failed_feature_files_lint
    exit 1
fi

