from .lxd import (
    get_dms_instances_on_lxd,
    exec_command_lxd_instance,
)
import signal
import functools
from behave.runner import Context


class TimeoutException(Exception):
    pass


def timeout_handler(seconds):
    def trigger_timeout(signum, frame):
        raise TimeoutException(f"Step timed out after {seconds} seconds")

    return trigger_timeout


def step_timeout_wrapper(seconds, func):
    @functools.wraps(func)
    def wrapped_func(*args, **kwargs):
        # Set the timeout handler
        signal.signal(signal.SIGALRM, timeout_handler(seconds))
        signal.alarm(seconds)

        try:
            return func(*args, **kwargs)
        finally:
            signal.alarm(0)  # Disable the alarm

    return wrapped_func


def before_all(context: Context):
    # Get all step implementations
    registry = context._runner.step_registry
    default_step_timeout = 30
    step_timeout = int(
        context.config.userdata.get("step_timeout", default_step_timeout)
    )

    # Apply timeout to all step implementations
    for _, matchers in registry.steps.items():
        for matcher in matchers:
            matcher.func = step_timeout_wrapper(step_timeout, matcher.func)


kill_dms_cmds = """
    pkill -TERM -x nunet && sleep 2; pkill -KILL -x nunet;
    pkill -f "nunet run" && sleep 2; pkill -KILL -f "nunet run";

    # *** IMPORTANT: For some reason, it only works if we keep the next lines:
    # TODO: remove the following and keep it functional
    if pgrep -x nunet > /dev/null || pgrep -f "nunet run" > /dev/null; then
        echo "Failed to kill nunet process" && ps aux | grep nunet
    else
        echo "nunet process successfully terminated"
    fi
"""

clean_nunet_dir_cmd = """
    rm -rf ~/.nunet/*
"""


def clean_nunet_dir(context):
    instances = context.instances
    ssh_key_file = context.config.userdata["ssh_key_file"]
    for instance in instances:
        exec_command_lxd_instance(
            instance,
            clean_nunet_dir_cmd,
            ssh_key_file,
        )


def before_feature(context, feature):
    print(f"Executing before feature hook: {feature.name}")
    if "inventory_file" in context.config.userdata:
        inventory_file = context.config.userdata["inventory_file"]

        # verify instances are correctly setup
        instances = get_dms_instances_on_lxd(inventory_file)
        context.instances = instances
        assert len(instances) > 0, "No DMS instances found in inventory"

        # initialize data structure for users
        if not hasattr(context, "users"):
            context.users = {}

        context.first_instance = instances[0]
        context.second_instance = instances[1]

        context.passphrase_key = "DMS_PASSPHRASE"
        context.passphrase_value = "test"


def before_scenario(context, scenario):
    if "skip" in scenario.tags:
        scenario.skip("Skipping...")
        return

    if "inventory_file" in context.config.userdata:
        instances = context.instances
        ssh_key_file = context.config.userdata["ssh_key_file"]

        for instance in instances:
            exec_command_lxd_instance(
                instance,
                kill_dms_cmds,
                ssh_key_file,
            )

        if "capability" in scenario.tags:
            clean_nunet_dir(context)
            context.users = {}

        for instance in instances:
            exec_command_lxd_instance(
                instance,
                "mkdir -p ~/.nunet",
                ssh_key_file,
            )

    if [tag for tag in scenario.effective_tags if "skip" == tag]:
        scenario.skip("tagged with 'skip'")


def after_scenario(context, scenario):
    # TODO (IMPORTANT): after_scenarion only runs when the scenario succeeds.
    # It must run on failure too!
    if "inventory_file" in context.config.userdata:
        inventory_file = context.config.userdata["inventory_file"]
        instances = get_dms_instances_on_lxd(inventory_file)
        ssh_key_file = context.config.userdata["ssh_key_file"]

        for instance in instances:
            exec_command_lxd_instance(
                instance,
                kill_dms_cmds,
                ssh_key_file,
            )

        if "capability" in scenario.tags:
            clean_nunet_dir(context)
            context.users = {}

    if any("AllureFormatter" in format for format in context.config.format):
        import allure

        log = context.log_capture.getvalue()
        stdout = context.stdout_capture.getvalue()
        stderr = context.stderr_capture.getvalue()

        allure.attach(log, name="log", attachment_type=allure.attachment_type.TEXT)
        allure.attach(
            stdout, name="stdout", attachment_type=allure.attachment_type.TEXT
        )
        allure.attach(
            stderr, name="stderr", attachment_type=allure.attachment_type.TEXT
        )
