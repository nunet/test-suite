#!/usr/bin/env bash

# Function to create LXC VM
create_vm() {
    local vm_name="$1"
    echo "Creating LXC VM: $vm_name"
    lxc launch ubuntu:22.04 $vm_name --vm --config=user.data="$(cat cloud-config.yml)" --config=limits.memory=3GiB

    for i in $(seq 1 60); do
        if lxc exec $vm_name -- sh -c "systemctl isolate multi-user.target" >/dev/null 2>/dev/null; then
            echo "VM creation complete"
            break
        fi
        if [ $((i % 5)) -eq 0 ]; then
            echo "Still configuring VM..."
        fi
        sleep 1s
    done
    lxc exec $vm_name -- sh -c "git config --global core.sshCommand 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'"

    echo "Cloning DMS"
    lxc exec $vm_name -- sh -c "git clone https://gitlab.com/nunet/device-management-service.git device-management-service"
    echo "Cloning SPD"
    lxc exec $vm_name -- sh -c "git clone https://gitlab.com/nunet/management-dashboard.git management-dashboard"
    echo "Cloning CPD"
    lxc exec $vm_name -- sh -c "git clone https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-webapp.git ml-on-gpu-webapp"

    echo "VM Setup complete."

}

# Function to stop LXC VM
stop_vm() {
    local vm_name="$1"
    echo "Stopping LXC VM: $vm_name"
    lxc stop "$vm_name"
}

# Function to cleanup LXC VM
cleanup() {
    local vm_name="$1"
    echo "Deleting LXC VM: $vm_name"
    lxc delete --force "$vm_name"
}

# Main function
main() {
    local vm_name="test-suite"
    local stop_requested=0

    # Parse command line arguments
    while [[ $# -gt 0 ]]; do
        case $1 in
            --vm-name)
                vm_name="$2"
                shift
                shift
                ;;
            --stop)
                stop_requested=1
                shift
                ;;
            *)
                echo "Invalid argument: $1"
                exit 1
                ;;
        esac
    done

    echo "LXC VM Management Script"
    echo "------------------------"

    if [[ $stop_requested -eq 1 ]]; then
        stop_vm "$vm_name"
        exit 0
    fi

    echo "1. Create VM"
    echo "2. Stop VM"
    echo "3. Cleanup VM"
    echo "4. Exit"

    read -rp "Enter your choice: " choice

    case $choice in
        1)
            create_vm "$vm_name"
            ;;
        2)
            stop_vm "$vm_name"
            ;;
        3)
            cleanup "$vm_name"
            ;;
        4)
            echo "Exiting..."
            exit 0
            ;;
        *)
            echo "Invalid choice. Exiting..."
            exit 1
            ;;
    esac
}

# Execute main function
main "$@"
