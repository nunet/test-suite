# Dependency scanning

Dependency Scanning is a feature that analyzes an application's dependencies for known vulnerabilities, including transitive dependencies.
It is part of Software Composition Analysis (SCA) and helps identify potential risks in your code before committing changes.
For more information on these features, please visit the GitLab page about [Dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/).
