@skip
Feature: Change or update Resource Configuration

  Background:
    Given dms lxd instances are up and running
    And dms service is active on the target machine
    And the API base URL is "http://localhost:9999/api/v1"


# Data models are located at platform-data-model/device-management-service/onboarding

#  Scenario: Change resources for an onboarded device successfully
#    Given the DMS is installed and the ResourceConfig endpoint is available - Done
#    And the device is onboarded. - Done
#    And the device has existing metadata - Done
#    And the request body contains valid resource configuration - Done
#    When a request is made to /onboarding/resource-config with the valid request body in "models.CapacityForNunet" format  -Done
#    Then 'device resource change started' message in "resourceChangeStart.message" format should be logged to Elasticsearch - ?? How to check this?
#    And the device capacity dedicated by the user should be validated (between 10% to 90%) - Done
#    And the existing metadata file should be read - How to check this
#    And the reserved CPU and memory in metadata should be updated - Done
#    Then read the existing database in "availableResources.data" format -?? What is the need of this step?
#    And update the 'TotCpuHz' and 'Ram' stored in the database - ?? Why to update the DB?
#    And save the updated values in the database -??  Why to update the DB?
#    Then save the updated metadata to the metadata path in "metadataV2.data" format - Why to update the metadata file? This should be done via API.
#    And log the message 'device resource change' in "successfulResourceChange.message" format to Elasticsearch - ?? How to check this?
#    Then free resources should be calculated and updated in the database - Can be automated. We can leave this as we already validating the resources in the response
#    Then the response status code should be 200 - Done
#    And the response should be in JSON format- Can be automated. But as per industry practice it is not need to check.
#    And the response should contain the updated metadata in "metadataV2.data" format - How to check this? We can check the API response.
#    And the response should not contain an error message - Done


#  ________________________________________________
# Moved from - api-tests/onboarding_api.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Verify the resource-config api response with 50% memory and 50% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 50% memory and 50% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "update_timestamp": {
            "type": "integer"
          },
          "resource": {
            "type": "object",
            "properties": {
              "memory_max": {
                "type": "integer"
              },
              "total_core": {
                "type": "integer"
              },
              "cpu_max": {
                "type": "integer"
              }
            },
            "required": [
              "memory_max",
              "total_core",
              "cpu_max"
            ]
          },
          "available": {
            "type": "object",
            "properties": {
              "cpu": {
                "type": "integer"
              },
              "memory": {
                "type": "integer"
              }
            },
            "required": [
              "cpu",
              "memory"
            ]
          },
          "reserved": {
            "type": "object",
            "properties": {
              "cpu": {
                "type": "integer"
              },
              "memory": {
                "type": "integer"
              }
            },
            "required": [
              "cpu",
              "memory"
            ]
          },
          "network": {
            "type": "string"
          },
          "public_key": {
            "type": "string"
          },
          "ntx_price": {
            "type": "number"
          }
        },
        "required": [
          "name",
          "update_timestamp",
          "resource",
          "available",
          "reserved",
          "network",
          "public_key",
          "ntx_price"
        ]
      }
      """
    And the value of reserved capacity in the response body should be same as onboarded capacity


  @setup_onboard
  Scenario: Verify the resource-config api response with 10% memory and 10% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 10% memory and 10% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the value of reserved capacity in the response body should be same as onboarded capacity


  @setup_onboard
  Scenario: Verify the resource-config api response with 90% memory and 90% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 90% memory and 90% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the value of reserved capacity in the response body should be same as onboarded capacity


  @setup_onboard
  Scenario: Verify the resource-config api response when the request body is empty
    Given I have the POST body from file "resources/post_bodies/empty.json"
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "invalid request data"


#  Scenario: Error when the device is not onboarded
#    Given the DMS is installed and the ResourceConfig endpoint is available - Done
#    And the device is not onboarded - Done
#    When a request is made to /onboarding/resource-config - Done
#    Then 'device resource change started' message in "resourceChangeStart.message" format should be logged to Elasticsearch - why and How to check this?
#    Then the response status code should be 400 - Done
#    And the response should be in JSON format - Can be automated. But as per industry practice it is not needed to check.
#    And the response should contain an error message about the machine not being onboarded - Done

#  ________________________________________________
# Moved from - api-tests/dms_api_wo_onboard.feature
#  ________________________________________________
  @setup_offboard
  Scenario: Verify the resource-config api response with 50% memory and 50% cpu when the device is not on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 50% memory and 50% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "500"
    And the response status message should be "Internal Server Error"
    And the value of "error" attribute in the response body should be "machine is not onboarded"


#  Scenario: Error when the request body contains invalid data
#    Given the DMS is installed and the ResourceConfig endpoint is available
#    And the device is onboarded
#    And the request body contains invalid data
#    When a request is made to /onboarding/resource-config with the invalid request body
#    Then 'device resource change started' message in "resourceChangeStart.message" format should be logged to Elasticsearch
#    Then the response status code should be 400
#    And the response should be in JSON format
#    And the response should contain an error message about invalid request data



#  ________________________________________________
# Moved from - api-tests/onboarding_api.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Verify the resource-config api response with 9% memory and 50% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 9% memory and 50% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "memory should be between 10% and 90% of the available memory"


  @setup_onboard
  Scenario: Verify the resource-config api response with 91% memory and 50% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 91% memory and 50% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "memory should be between 10% and 90% of the available memory"


  @setup_onboard
  Scenario: Verify the resource-config api response with 50% memory and 9% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 50% memory and 9% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "CPU should be between 10% and 90% of the available CPU"


  @setup_onboard
  Scenario: Verify the resource-config api response with 50% memory and 91% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/resource-config.json"
    When I update the POST body with 50% memory and 91% cpu
    When a POST request is made to the "onboarding/resource-config" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "CPU should be between 10% and 90% of the available CPU"


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when metadata file cannot be read
    Given the DMS is installed and the ResourceConfig endpoint is available
    And the device is onboarded
    And there is an error reading the metadata file
    When a request is made to /onboarding/resource-config
    Then 'device resource change started' message in "resourceChangeStart.message" format should be logged to Elasticsearch
    Then the response status code should be 400
    And the response should be in JSON format
    And the response should contain an error message about not being able to read metadata


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when metadata file cannot be written
    Given the DMS is installed and the ResourceConfig endpoint is available
    And the device is onboarded
    And the metadata file cannot be written
    When a request is made to /onboarding/resource-config
    Then 'device resource change started' message in "resourceChangeStart.message" format should be logged to Elasticsearch
    Then the response status code should be 400
    And the response should be in JSON format
    And the response should contain an error message about not being able to write metadata.json


#Que - Need understanding about how to reproduce the scenario?
# Recommendation - Such scenario should not be automated.
  @manual @snba
  Scenario: Error when calculating and updating free resources
    Given the DMS is installed and the ResourceConfig endpoint is available
    And the device is onboarded
    And there is an error calculating and updating free resources
    When a request is made to /onboarding/resource-config
    Then 'device resource change started' message in "resourceChangeStart.message" format should be logged to Elasticsearch
    Then the response status code should be 200
    And the response should be in JSON format
    And the response should contain the updated metadata in "metadataV2.data" format
    And the reserved capacity in metadata should match the provided resource configuration
    And the available resources in the database should match the provided resource configuration
    And the metadataV2.json file should be updated with the new resources
    And log the message 'device resource change' in "successfulResourceChange.message" format to Elasticsearch
    And an error message about the free resource calculation should be logged
