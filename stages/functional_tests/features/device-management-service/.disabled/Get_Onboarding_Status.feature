@skip
Feature: Check Onboarding Status 

# Data models are located at platform-data-model/device-management-service/onboarding

  Background:
    Given the API base URL is "http://localhost:9999/api/v1"



#  Scenario: Retrieve onboarding status and metadata successfully
#    Given the DMS is installed and the Status endpoint is available -Done
#    When a request is made to /onboarding/status - Done
#    Then Check if metadata can be read - How to check this?
#    And Check if libp2p private key is not NIL - How to check this?
#    Then set onboarding status - What we want to test in this step?
#    Then check that file exists at metadata path - Which file? Is it metadata.json file? If yes, we can do but is it required to check?
#    And check that file exists at database path - Can be automated. We can leave this as we already validating the resources in the response
#    Then the response status code should be 200 - Done
#    And the response should be in JSON format - Can be automated. But as per industry practice it is not need to check.
#    And the response should be in "onboardingStatus.message" format - How to check this? We are checking onboarding status in the API response.


#  ________________________________________________
# Moved from - api-tests/onboarding_api.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Verify the GET onboarding/status api response when the device is on-boarded
    When a GET request is made to the "onboarding/status" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
          "onboarded": {
            "type": "boolean"
          },
          "error": {
            "type": "null"
          },
          "machine_uuid": {
            "type": "string"
          },
          "metadata_path": {
            "type": "string"
          },
          "database_path": {
            "type": "string"
          }
        },
        "required": [
          "onboarded",
          "error",
          "machine_uuid",
          "metadata_path",
          "database_path"
        ]
      }
      """
    And the value of "onboarded" attribute in the response body should be "True"
    And "metadata_path" attribute in the response body should be non empty


#  ________________________________________________
# Moved from - api-tests/dms_api_wo_onboard.feature
#  ________________________________________________
  @setup_offboard
  Scenario: Verify the GET onboarding/status api response when the device is not on-boarded
    When a GET request is made to the "onboarding/status" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "properties": {
          "onboarded": {
            "type": "boolean"
          },
          "error": {
            "type": "null"
          },
          "machine_uuid": {
            "type": "string"
          },
          "metadata_path": {
            "type": "string"
          },
          "database_path": {
            "type": "string"
          }
        },
        "required": [
          "onboarded",
          "error",
          "machine_uuid",
          "metadata_path",
          "database_path"
        ]
      }
      """
    And the value of "onboarded" attribute in the response body should be "False"
