@skip
Feature: Get Provisioned Capacity 

# Data models are located at platform-data-model/device-management-service/onboarding
  Background:
    Given dms lxd instances are up and running
    And dms service is active on the target machine
    And the API base URL is "http://localhost:9999/api/v1"


#  Scenario: Retrieve provisioned capacity successfully
#    Given the DMS is installed and ProvisionedCapacity endpoint is available - Done
#    When a request is made to /onboarding/provisioned - Done
#    Then the response status code should be 200 - Done
#    And the response should be in JSON format - Can be automated. But as per industry practice it is not need to check.
#    And the response should contain the provisioned capacity in "provisioned.data" format - Done

#  ________________________________________________
# Moved from - api-tests/onboarding_api.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Verify the provisioned api response when the device is on-boarded
    When a GET request is made to the "onboarding/provisioned" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "object",
        "properties": {
          "cpu": {
            "type": "number"
          },
          "memory": {
            "type": "integer"
          },
          "total_cores": {
            "type": "integer"
          }
        },
        "required": [
          "cpu",
          "memory",
          "total_cores"
        ]
      }
      """
    When I captured the endpoint result
    When I get the system full capacity using linux native commands
    Then nunet command result should match with native linux command result


#  ________________________________________________
# Moved from - cli-tests/nunet_cli.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Check the nunet full system capacity command when device is onboarded
    When the user execute the command "nunet capacity --full" with user input "empty"
    When the nunet command output is processed to get the system parameters
    When I get the system full capacity using linux native commands
    Then nunet command result should match with native linux command result


#  ________________________________________________
# Moved from - api-tests/dms_api_wo_onboard.feature
#  ________________________________________________
  @setup_offboard
  Scenario: Verify the provisioned api response when the device is not on-boarded
    When a GET request is made to the "onboarding/provisioned" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "object",
        "properties": {
          "cpu": {
            "type": "number"
          },
          "memory": {
            "type": "integer"
          },
          "total_cores": {
            "type": "integer"
          }
        },
        "required": [
          "cpu",
          "memory",
          "total_cores"
        ]
      }
      """
    When I captured the endpoint result
    When I get the system full capacity using linux native commands
    Then nunet command result should match with native linux command result


#  ________________________________________________
# Moved from - cli-tests/nunet_cli_wo_onboard.feature
#  ________________________________________________
  @setup_offboard
  Scenario: Check the nunet full system capacity command when device is not onboarded
    When the user execute the command "nunet capacity --full" with user input "empty"
    When the nunet command output is processed to get the system parameters
    When I get the system full capacity using linux native commands
    Then nunet command result should match with native linux command result
