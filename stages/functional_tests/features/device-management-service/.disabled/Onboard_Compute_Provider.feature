@skip
Feature: Onboarding Compute Provider to Nunet network

  Background:
    Given dms lxd instances are up and running
    And dms service is active on the target machine
    And the API base URL is "http://localhost:9999/api/v1"


# Data models are located at platform-data-model/device-management-service/onboarding

#  The compute provider user has to onboard its machines to the network before
#  it can be used to run any task. The user specifies the resources,
#  payment address and network channel.
#
#
#  Scenario: User onboards the machine
#    Given the DMS is installed and the onboarding component is running - Done
#    When a POST request is made to "/onboarding/onboard" with JSON body in the "capacityForNunet.data" format - Done
#    Then check metadata path - ?? Not in Gherkin format. I assume that we are validating the response schema. If yes, I am validating this.
#    * get hostname and timestamp
#    * verify the total device capacity - CPU, Memory & number of cores - in "provisioned.data" format
#    * generate metadata in "metadataV2.data" format
#    * store the details captured in above steps in metadata
#    * validate the payment address
#    * verify the device capacity onboarded in the POST request (should be between 10% - 90% of total capacity)
#
#    When capacityForNunet.Cardano is True - How to test this
#    And device memory & CPU exceed the minimum requirements for a Cardano node
#    Then set metadata.AllowCardano as True
#
#    When capacityForNunet.Cardano is False, set metadata.AllowCardano as false - How to test this
#
#    Then detect GPU and save the GPU parameters in metadata in "models.gpuInfo" format - How to test this
#
#    Then validate the network channel entered by the user - I assume that we are testing the onboarding with defined range of cpu and memory. If yes, I am validating this.
#    * calculate available (free) resources
#    * save metadata to metadata path
#    * add the availalbe resources to database in "availableResources.data" format
#    * generarte libp2p key and save node info
#    * start libp2p node
#
#    # receives a string as token. This is saved in database in "newElasticToken.data" format - ?? Seems it's more to test the data flow from developers perspective
#    * get a new telemetry token from Elasticsearch using "getToken.message" as input
#    * send "device onboarded" message in "successfulOnboarding.message" format to ELK
#
#    # receives a string as token. This is saved in database in "logbinAuth.data" format
#    * register the device with logbin using "registerLogbin.message" as input
#
#    * return the request with response status code 200 and metadata

#  ________________________________________________
# Moved from - api-tests/onboarding_api.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Verify the on-board api response with 50% memory and 50% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 50% memory and 50% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "update_timestamp": {
            "type": "integer"
          },
          "resource": {
            "type": "object",
            "properties": {
              "memory_max": {
                "type": "integer"
              },
              "total_core": {
                "type": "integer"
              },
              "cpu_max": {
                "type": "integer"
              }
            },
            "required": [
              "memory_max",
              "total_core",
              "cpu_max"
            ]
          },
          "available": {
            "type": "object",
            "properties": {
              "cpu": {
                "type": "integer"
              },
              "memory": {
                "type": "integer"
              }
            },
            "required": [
              "cpu",
              "memory"
            ]
          },
          "reserved": {
            "type": "object",
            "properties": {
              "cpu": {
                "type": "integer"
              },
              "memory": {
                "type": "integer"
              }
            },
            "required": [
              "cpu",
              "memory"
            ]
          },
          "network": {
            "type": "string"
          },
          "public_key": {
            "type": "string"
          },
          "ntx_price": {
            "type": "number"
          }
        },
        "required": [
          "name",
          "update_timestamp",
          "resource",
          "available",
          "reserved",
          "network",
          "public_key",
          "ntx_price"
        ]
      }
      """
    And the value of reserved capacity in the response body should be same as onboarded capacity


  @setup_onboard
  Scenario: Verify the on-board api response with 10% memory and 10% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 10% memory and 10% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the value of reserved capacity in the response body should be same as onboarded capacity


  @setup_onboard
  Scenario: Verify the on-board api response with 90% memory and 90% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 90% memory and 90% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the value of reserved capacity in the response body should be same as onboarded capacity


  @setup_onboard
  Scenario: Verify the on-board api response with 9% memory and 50% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 9% memory and 50% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "memory should be between 10% and 90% of the available memory"


  @setup_onboard
  Scenario: Verify the on-board api response with 91% memory and 50% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 91% memory and 50% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "memory should be between 10% and 90% of the available memory"


  @setup_onboard
  Scenario: Verify the on-board api response with 50% memory and 9% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 50% memory and 9% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "CPU should be between 10% and 90% of the available CPU"


  @setup_onboard
  Scenario: Verify the on-board api response with 50% memory and 91% cpu when the device is already on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 50% memory and 91% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "400"
    And the response status message should be "Bad Request"
    And the value of "error" attribute in the response body should be "CPU should be between 10% and 90% of the available CPU"


#  ________________________________________________
# Moved from - api-tests/dms_api_wo_onboard.feature
#  ________________________________________________
  @setup_offboard
  Scenario: Verify the on-board api response with 50% memory and 50% cpu when the device is not on-boarded
    When I get the system full capacity using linux native commands
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When I update the POST body with 50% memory and 50% cpu
    When a POST request is made to the "onboarding/onboard" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "update_timestamp": {
            "type": "integer"
          },
          "resource": {
            "type": "object",
            "properties": {
              "memory_max": {
                "type": "integer"
              },
              "total_core": {
                "type": "integer"
              },
              "cpu_max": {
                "type": "integer"
              }
            },
            "required": [
              "memory_max",
              "total_core",
              "cpu_max"
            ]
          },
          "available": {
            "type": "object",
            "properties": {
              "cpu": {
                "type": "integer"
              },
              "memory": {
                "type": "integer"
              }
            },
            "required": [
              "cpu",
              "memory"
            ]
          },
          "reserved": {
            "type": "object",
            "properties": {
              "cpu": {
                "type": "integer"
              },
              "memory": {
                "type": "integer"
              }
            },
            "required": [
              "cpu",
              "memory"
            ]
          },
          "network": {
            "type": "string"
          },
          "public_key": {
            "type": "string"
          },
          "ntx_price": {
            "type": "number"
          }
        },
        "required": [
          "name",
          "update_timestamp",
          "resource",
          "available",
          "reserved",
          "network",
          "public_key",
          "ntx_price"
        ]
      }
      """
    And the value of reserved capacity in the response body should be same as onboarded capacity


#  ________________________________________________
# Moved from - cli-tests/nunet_cli.feature
#  ________________________________________________
  @setup_onboard
  Scenario: Check nunet off-boarding with user input "no"
    When the user execute the command "nunet offboard" with user input "no"
    Then the command should exit with message "Exiting..."


  @setup_onboard
  Scenario: Re-onboard the device with 50% memory and 70% CPU with user input "yes"
    When I get the system full capacity using linux native commands
    When I run nunet onboard command with 50% memory and 70% CPU with user input "yes"
    Then the command should exit with message "Sucessfully onboarded"


  @setup_onboard
  Scenario: Check nunet off-boarding with user input "yes"
    When the user execute the command "nunet offboard" with user input "yes"
    Then the command should exit with message "successfully offboarded"


#  ________________________________________________
# Moved from - cli-tests/nunet_cli_wo_onboard.feature
#  ________________________________________________

#  ___________________________________________________________________________________________________________________
#    The Scenario become invalid due to bug - https://gitlab.com/nunet/device-management-service/-/issues/415
#  Depending  upon the bug decision , the scenario either will be removed or adapt.
#  ___________________________________________________________________________________________________________________
#  @setup_offboard
#  Scenario: Run nunet onboard with 30% memory and 40% CPU with user input "no"
#    When I get the system full capacity using linux native commands
#    When I run nunet onboard command with 30% memory and 40% CPU with user input "no"
#    Then the command should exit with message "Exiting.."

  @setup_offboard
  Scenario: Run nunet onboard with 50% memory and 40% CPU with user input "yes"
    When I get the system full capacity using linux native commands
    When I run nunet onboard command with 50% memory and 40% CPU with user input "yes"
    Then the command should exit with message "Sucessfully Onboarded"


  @setup_offboard
  Scenario: Fresh onboard the device with 50% memory and 50% CPU with user input "yes"
    When I get the system full capacity using linux native commands
    When I run nunet onboard command with 50% memory and 50% CPU with user input "yes"
    Then the command should exit with message "Sucessfully Onboarded"
