@skip
Feature: Dealing with multiple dms


  Scenario: Check if i can get the peer id of desired dms instance running on lxd vm
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When i onboard all the dms instances via api
    When sending a GET api call to "/api/v1/peers/self" on 0 dms instance
    Then the response status code should be 200
    And each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters
    And the response should have the following structure
      """
      {
        "type": "object",
        "properties": {
          "ID": {"type": "string"},
          "Addrs": {
            "type": "array",
            "items": {"type": "string"}
          }
        },
        "required": ["ID", "Addrs"]
      }
      """
    And each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters
    And each peer in the response should have a non-empty list of addresses
    And each address in the response should be a valid IP address with a port
    When sending a GET api call to "/api/v1/peers/self" on 1 dms instance
    Then the response status code should be 200
    And each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters
    And each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters
    And each peer in the response should have a non-empty list of addresses
    And each address in the response should be a valid IP address with a port


  Scenario: Check if i can independently onboard/offboard desired dms instance running on lxd vm
    Given I have the POST body from file "resources/post_bodies/onboard.json"
    When i onboard all the dms instances via api
    When sending a GET api call to "/api/v1/onboarding/status" on 0 dms instance
    Then the value of "onboarded" attribute in the response body should be "True"
    When sending a DELETE api call to "/api/v1/onboarding/offboard" on 1 dms instance
    When sending a GET api call to "/api/v1/onboarding/status" on 1 dms instance
    Then the value of "onboarded" attribute in the response body should be "False"
