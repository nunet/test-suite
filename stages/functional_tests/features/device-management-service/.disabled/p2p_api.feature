Feature: DMS P2P API Testing

  Background:
    Given the API base URL is "http://localhost:9999/api/v1"

  Scenario: Verify the GET peers api response
    When a GET request is made to the "peers" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "ID": {"type": "string"},
            "Addrs": {
              "type": "array",
              "items": {"type": "string"}
            }
          },
          "required": ["ID", "Addrs"]
        }
      }
      """
    And each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters
    And each peer in the response should have a non-empty list of addresses
    And each address in the response should be a valid IP address with a port

  Scenario: Verify the GET peers/self api response
    When a GET request is made to the "peers/self" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "object",
        "properties": {
          "ID": {"type": "string"},
          "Addrs": {
            "type": "array",
            "items": {"type": "string"}
          }
        },
        "required": ["ID", "Addrs"]
      }
      """
    And each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters
    And each peer in the response should have a non-empty list of addresses
    And each address in the response should be a valid IP address with a port

  Scenario: Verify the GET peers/dht api response
    When a GET request is made to the "peers/dht" endpoint
    Then the response status code should be "200"
    And the response status message should be "OK"
    And the response should have the following structure
      """
      {
        "type": "array",
        "items": {
        "type": "string"
        }
      }
      """
    And each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters
