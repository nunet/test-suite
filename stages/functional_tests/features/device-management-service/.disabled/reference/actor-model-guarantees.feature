@wip
Feature: Actor Model Guarantees
   Actor model implementation provides specific computational guarantees
   Which are enforced in the nuActor system

# Actors can communicate with each other only via messages #
# Private state of actors is not directly accessible to other actors #
# Actors can only communicate with other actors that are in the same nuActor system #
# Enables concurrency and non-locking by design which is independent of the scale of the platform at any point in time


