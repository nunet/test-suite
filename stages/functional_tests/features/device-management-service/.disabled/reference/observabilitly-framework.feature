@wip
Feature: Observability Framework
   All events in the system can be observed locally or remotely
   By defining unified event types, log levels and collector sinks
   Which can be configured as needed dynamically

# observability;   
# open tracing;
# metrics;
# dynamic change of log levels;
# dynamic change of collector sinks;

