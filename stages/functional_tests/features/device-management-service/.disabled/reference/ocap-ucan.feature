@wip
Feature: User Controlled Authorization Network
   nuActor system extends the UCAN standard
   For implementing Object Capability Programming and security model

# users of the system are uniquely identified by their DID
# all nuActors (nodes, orchestrators, allocations) are uniquely identified by their ID
# all messages between nuActors are authenticated and encrypted
# [get all properties of OCaps and implement as tests]