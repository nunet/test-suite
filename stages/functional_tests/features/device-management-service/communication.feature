Feature: Communication between deployed allocations
    As a service provider I want to be sure
    That deployed ensemble workers and allocations
    Can communicate with each other irrespectively of network configurations

    ## please specify all scenarios as needed
    ## either in Gherkin syntax or in plain english as comments;

    Background:
        Given Bob is the service provider deploying Alice's and Sam's allocations
        And Bob ensures that all deployed allocations are configured for communication
        And Sam has a running allocation
        And Alice has a running allocation


    # Examples:
    #         | one_alloc.yaml           |
    #         | two_alloc_same_node.yaml |
    #         | two_alloc_two_node.yaml  |
    
    @wip @capability
    @standalone
    Scenario: Ensure deployment communication for various configurations
        Given Alice's allocation "one_alloc.yaml"  is running
        When Alice attempts to communicate with her own allocation services
        Then Alice's allocation should send and receive internal messages successfully

    @wip @capability
    @distributed
    Scenario: Ensure Alice can send a message to Sam on the same node
        Given Alice's allocation is deployed on the same node as Sam's allocation
        When Alice resolves Sam's network address
        And Alice sends a message to Sam
        Then Sam should receive the message

    @wip @capability
    @distributed
    Scenario: Ensure Alice can receive a message from Sam on the same node
        Given Sam's allocation is deployed on the same node as Alice's allocation
        When Sam resolves Alice's network address
        And Sam sends a message to Alice
        Then Alice should receive the message

    @wip @capability
    @distributed
    Scenario: Ensure Alice can send a message to Sam on different nodes
        Given Alice's allocation is deployed on one node and Sam's allocation is deployed on another node
        When Alice resolves Sam's network address
        And Alice sends a message to Sam
        Then Sam should receive the message

    @wip @capability
    @distributed
    Scenario: Ensure Sam can send a message to Alice on different nodes
        Given Sam's allocation is deployed on one node and Alice's allocation is deployed on another node
        When Sam resolves Alice's network address
        And Sam sends a message to Alice
        Then Alice should receive the message

    @wip @capability
    @distributed
    Scenario: Verify dynamic address resolution across subnets
        Given Alice's allocation and Sam's allocation are deployed in different subnets
        When Alice resolves Sam's network address dynamically
        Then Alice should establish a connection with Sam 

    @wip @capability
    @distributed
    Scenario: Ensure communication continues after network configuration changes
        Given Alice and Sam's allocations are deployed in separate networks or subnets
        And Bob modifies the network to move Alice and Sam to a shared subnet
        When Alice attempts to ping Sam's allocation after the network change
        Then Alice should successfully ping Sam 


        @wip @capability
    @standalone
    Scenario: Ensure peer-to-peer communication through a messaging system
        Given Alice has registered a handler for messages of type "worker-message"
        And Sam sends a message of type "worker-message" to Alice
        When Alice receives the message from Sam
        Then Alice should process the message successfully

    @wip @capability
    @standalone
    Scenario: Ensure communication channel remains open for further messages
        Given Alice has processed the message successfully
        When Alice waits for additional messages
        Then Alice should be able to receive further messages from Sam

   
    @wip @capability
    @distributed
    Scenario: Verify communication for two allocations with a single ensemble deployment
        Given Alice and Sam have deployed allocations "two_alloc_two_node.yaml" as part of a single ensemble
        When Alice attempts to send messages to Sam within the ensemble
        Then Alice and Sam should be able to communicate without network configuration restrictions

    @wip @capability
    @distributed
    Scenario: Communication fails due to PeerID resolution issues
        Given Alice's allocation is deployed 
        And Sam's allocation is deployed 
        When Alice resolves Sam's PeerID "peerInvalid"
        Then Alice should fail to retrieve Sam's network address

    @wip @capability
    @distributed
    Scenario: Communication fails due to timeout
        Given Alice's allocation is deployed on node 
        And Sam's allocation is deployed on another node
        When Alice attempts to ping Sam's network address with a timeout limit
        Then Alice should fail to establish communication

    @wip @capability
    @distributed
    Scenario: Communication fails due to mismatched subnets
        Given Alice's allocation is deployed on subnet "subnetA"
        And Sam's allocation is deployed on subnet "subnetB"
        When Alice attempts to communicate with Sam without dynamic address resolution
        Then Alice should fail to establish communication

    @wip @capability
    @distributed
    Scenario: Ensure recovery after unexpected network disruption
        Given Alice and Sam's allocations are actively communicating
        And a temporary network outage occurs
        When the network is restored
        Then Alice and Sam should re-establish communication without manual intervention

    @wip @capability
    @distributed
    Scenario: Communication fails if no handler is registered
        Given Alice's allocation is deployed 
        And Sam sends a message of type "worker-message" to Alice
        And Alice has not registered a handler for "worker-message"
        When Sam's message reaches Alice's allocation
        Then Alice should discard the message without processing it
