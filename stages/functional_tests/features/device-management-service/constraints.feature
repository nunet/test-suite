Feature: All constraints specified in ensemble are considered
    As a service provider I want to make sure
    That all constraints specified by users in ensemble specification files
    Are correctly considered and respected during deployment process
## please put all scenarios here in Gherkin syntax
## or plain English as such comments

  Background:
    Given Alice is a Service Provider
    And Bob is a Compute Provider
    And Charlie is a Compute Provider
    And Alice and Bob trust each other
    And Alice and Charlie trust each other
    And Alice is connected to Bob
    And Alice is connected to Charlie

  @wip
  Scenario: Single allocation with unmet RAM constraint
    Given Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has not have enough RAM as defined in the ensemble
    When Alice deploys "one_alloc.yaml" ensemble
    Then the ensemble execution should happen on Bob

  @wip
  Scenario: Single allocation with unmet CPU constraint
    Given Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has not have meet the CPU core requirement as defined in the ensemble
    When Alice deploys "one_alloc.yaml" ensemble
    Then the ensemble execution should happen on Bob

  @wip
  Scenario: Single allocation with unmet disk constraint
    Given Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has not have meet the disk space requirement as defined in the ensemble
    When Alice deploys "one_alloc.yaml" ensemble
    Then the ensemble execution should happen on Bob

  @wip
  Scenario: One allocation with location constraint by Service Provider
    Given Bob is in location B
    And Charlie is in location A
    And Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has enough resources to run the allocations defined in the ensemble
    When Alice deploys "one_alloc_locA.yaml" ensemble with location A constraint
    Then the ensemble execution should happen on Charlie

  @wip
  Scenario: One allocation with location constraint by Compute Provider
    Given Bob accepts jobs only from service providers in location A
    And Charlie accepts jobs only from service providers in location B
    And Alice is in location A
    And Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has enough resources to run the allocations defined in the ensemble
    When Alice deploys "one_alloc.yaml" ensemble
    Then the ensemble execution should happen on Bob

  @wip
  Scenario: One allocation with no nodes found
    Given Bob does not have enough {resource} as defined in the ensemble
    And Charlie does not have enough {resource} as defined in the ensemble
    When Alice deploys "one_alloc.yaml" ensemble
    Then the orchestrator should return an error

  @wip
  Scenario: Two allocation on same node with location constraint
    Given Bob is in location B
    And Charlie is in location A
    And Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has enough resources to run the allocations defined in the ensemble
    When Alice deploys "two_alloc_locA.yaml" ensemble with location A constraint
    Then the ensemble execution should happen on Charlie with two allocations

  @wip
  Scenario: Two allocation on two node with location constraint
    Given Bob is in location B
    And Charlie is in location A
    And Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has enough resources to run the allocations defined in the ensemble
    When Alice deploys "two_alloc_locAB.yaml" ensemble with location A constraint for allocation 1 & location B constraint for allocation 2
    Then the allocation for allocation 2 should happen on Bob and allocation 1 should happen on Charlie

  @wip
  Scenario: Error with Two allocation on two node with location constraint
    Given Bob is in location B
    And Charlie is in location A
    And Bob has enough resources to run the allocations defined in the ensemble
    And Charlie has enough resources to run the allocations defined in the ensemble
    When Alice deploys "two_alloc_locAB.yaml" ensemble with location A constraint for allocation 1 & location C constraint for allocation 2
    Then the orchestrator should return an error

  @wip
  Scenario: Two allocation on two node
    Given Bob has enough resources to run the allocations defined in the ensemble
    And Charlie does not have enough {resource} as defined in the ensemble
    When Alice deploys "two_alloc.yaml" ensemble
    Then the orchestrator should return an error indicating that there are not enough resources to run the allocations defined in the ensemble
