Feature: Ensemble deployment
	As a service provider
	I want to run an ensemble via NuNet network

  Background:
    Given Alice is a Service Provider
    And Bob is a Compute Provider
    And Charlie is a Compute Provider
    And Alice trusts Bob
    And Alice trusts Charlie
    And Alice is connected to Bob
    And Alice is connected to Charlie
		# TODO:
		# And Bob has enough resources to run the allocations defined in the ensemble
		# And Charlie has enough resources to run the allocations defined in the ensemble

  @wip @capability
  Scenario: One allocation
    Given Alice deploys "one_alloc.yaml" ensemble
    When the ensemble execution is completed
    Then the output of each allocation should contain "Hello from Docker!"

  @wip @capability
  Scenario: Two allocations on the same node
    Given Alice deploys "two_alloc_one_node.yaml" ensemble
    When all the allocations in the ensemble have been completed
    Then the output of each allocation should contain "Hello from Docker!"

  @wip @capability
  Scenario: Two allocations on different nodes
    Given Alice deploys "two_alloc_two_node.yaml" ensemble
    When all the allocations in the ensemble have been completed
    Then the output of each allocation should contain "Hello from Docker!"
