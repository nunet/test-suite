Feature: hello world

  @standalone
  Scenario: echo hello world in vm over ssh
    Given we have a target virtual machine
    When I execute "echo hello world" over ssh
    Then "hello world" should be printed to "stdout"
