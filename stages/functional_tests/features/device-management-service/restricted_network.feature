Feature: Restricted networks creation and management
    As a service provider
    I want to be able
    To create and manage a restricted network
    Which allows only actors with explicitly provided capabilities

    Background:
        Given Alice is managing restricted network 
        And Bob is an actor
        And charlie is an actor
        And Alice trusts Bob
        And Alice trusts Charlie
        And Bob and Charlie both have their contexts and capabilities onboarded

    @wip @capability
    @standalone
    Scenario: Single allocation in a restricted network
        Given Alice sets up the capability pool 
        And Bob requests an "one_alloc.yaml" deployment in a restricted network
        When Alice validates Bob's capabilities for deployment
        Then the deployment is successfully allocated on one node

    @wip @capability
    @distributed
    Scenario: Two allocations on two different nodes in a restricted network
        Given Alice sets up the capability pool
        And Charlie  requests "two_alloc_two_node.yaml" deployments in a restricted network
        And Bob  requests "two_alloc_two_node.yaml" deployments in a restricted network
        When Alice validates Charlie's and bob's capabilities for deployment
        Then the deployments are successfully allocated on two different nodes

    @wip @capability
    @distributed
    Scenario: Two allocations on the same node in a restricted network
        Given Alice sets up the capability pool 
        And Bob requests "two_alloc_node.yaml" deployments in a restricted network
        When Alice validates Bob's capabilities for deployment
        Then the deployments are successfully allocated on the same node

    @wip @capability
    @standalone
    Scenario: Deallocation of resources after allocation completion
        Given Bob has completed a single allocation deployment in the restricted network
        When the allocated deployment is marked as completed
        Then the resources used for the deployment should be deallocated

    @wip @capability
    @distributed
    Scenario Outline: Deployment request from an unverified user
        Given Alice sets up the capability pool 
        And Eve, an unverified user, requests a deployment
        When Alice checks Eve's capabilities for deployment
        Then the deployment should be rejected due to lack of valid capabilities

    @wip @capability
    @standalone
    Scenario: Deployment request with expired capabilities
        Given Alice grants Bob a capability token with expiry "2023-12-31"
        And the current date is "2024-01-01"
        When Bob requests a deployment
        Then the deployment should be rejected due to expired capabilities


    @wip @capability
    @standalone
    Scenario: Deployment on nodes outside the restricted network
        Given Bob has a valid capability token
        And Bob requests a deployment
        When the request is sent to a node outside restricted network
        Then the deployment should be rejected due to lack of trusted  nodes

    @wip @capability
    Scenario: Conflicting deployment requests
        Given Bob and Charlie both request " two_alloc_one_node.yaml" deployments on the same node
        And the node does not have sufficient resources for both
        When the requests are processed
        Then the node should prioritize the first valid request received


    @wip @capability
    Scenario: Resource deallocation failure
        Given Bob's deployment has completed
        And the node has a resource deallocation error
        When the deallocation is attempted
        Then the system should log the error 

    @wip @capability
    Scenario: Deallocation of resources after allocation completion
        Given Bob has completed a " one_alloc.yaml " deployment in the restricted network
        When the allocated deployment is marked as completed
        Then the resources used for the deployment should be deallocated

    @wip @capability
    Scenario: Cross-network deployment
        Given Bob has configured capabilities for both restricted network and NuNet networks
        And Bob requests a deployment
        When the deployment request is processed
        Then the deployment should be executed on the appropriate network based on the token used
