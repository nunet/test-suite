import yaml

one_alloc = """
version: "V1"

allocations:
    alloc1:
        executor: docker
        resources:
            cpu:
                cores: 1
            gpus: []
            ram:
                size: 1024
            disk:
                size: 20
        execution:
            type: docker
            image: hello-world
        dnsname: mydocker
nodes:
    node1:
        allocations:
            - alloc1
"""

two_allocs_one_node = """
version: "V1"

allocations:
    alloc1:
        executor: docker
        resources:
            cpu:
                cores: 1
            gpus: []
            ram:
                size: 1024
            disk:
                size: 20
        execution:
            type: docker
            image: hello-world
        dnsname: mydocker
    alloc2:
        executor: docker
        resources:
            cpu:
                cores: 1
            gpus: []
            ram:
                size: 1024
            disk:
                size: 20
        execution:
            type: docker
            image: hello-world
        dnsname: mydocker
nodes:
    node1:
        allocations:
            - alloc1
            - alloc2
"""

two_allocs_two_nodes = """
version: "V1"

allocations:
    alloc1:
        executor: docker
        resources:
            cpu:
                cores: 1
            gpus: []
            ram:
                size: 1024
            disk:
                size: 20
        execution:
            type: docker
            image: hello-world
        dnsname: mydocker
nodes:
    node1:
        allocations:
            - alloc1
    node2:
        allocations:
            - alloc1
"""

ensembles = {
        "one_alloc.yaml": one_alloc,
        "two_allocs_one_node.yaml": two_allocs_one_node,
        "two_allocs_two_nodes.yaml": two_allocs_two_nodes
}

def select_peer(ensemble, node, peer_id):
    if ensemble not in ensembles:
        raise Exception("ensemble not defined")
    data = yaml.safe_load(ensembles[ensemble])
    if "nodes" in data and node in data["nodes"]:
        data["nodes"][node]["peer"] = peer_id
    return data
