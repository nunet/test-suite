from lib.python.behave.lxd import exec_cmd
from behave import given, then, when
import ipaddress


@given("we have a target virtual machine")
def get_target_machine(context):
    instance = context.instances[0]
    try:
        ip_address = ipaddress.ip_address(instance)
        assert ip_address.is_private, f"expected {instance} to be a private ipaddress"
    except:
        print(f"expected {instance} to be an IP")
        raise


@when('I execute "{shell_cmd}" over ssh')
def execute_shell_over_ssh(context, shell_cmd):
    instance = context.instances[0]

    out = exec_cmd(context, instance, shell_cmd)

    context.output = out


@then('"{expected_output}" should be printed to "{stream_name}"')
def check_shell_output(context, expected_output, stream_name):
    actual_output = getattr(context.output, stream_name).strip()
    assert (
        actual_output == expected_output
    ), f"expected '{expected_output}', got '{actual_output}'"
