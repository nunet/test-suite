from lib.python.behave.lxd import exec_cmd, assert_cmd, exec_command_lxd_instance
from stages.functional_tests.steps.cli import anchor, grant, run, delegate
from stages.functional_tests.steps.ensemble import select_peer
import ipaddress
import json
import backoff


def is_node(name):
    """Check if a name represents a node (ends with -dms) or a user.
    Returns True if it's a node, False if it's a user.
    """
    return name.endswith("-dms")


def parse_name_literal(expr):
    """Convert expressions like "Alice's node" to "alice-dms" or "Alice" to "alice"

    Args:
        expr (str): Input string like "Alice's node" or "Alice" or "Alice's"

    Returns:
        str: Normalized name, either "alice-dms" or "alice"
    """
    name = expr.lower().replace("'s node", "").replace("'s", "")
    if "'s node" in expr:
        return f"{name}-dms"
    return name


def parse_name(expr):
    """
    tuple: (user_name, node_name) where user_name is lowercase without 's
           and node_name is user_name-dms
    """
    user = expr.lower().replace("'s node", "").replace("'s", "")
    node = f"{user}-dms"
    return user, node


def init_user_data(context, instance, user):
    if user not in context.users:
        context.users[user] = {
            "instance": instance,
            "did": "",
            "nodes": {},
            "caps": {},
        }


def init_node_data(context, instance, user, node):
    if node not in context.users[user]["nodes"]:
        context.users[user]["nodes"][node] = {
            "instance": instance,
            "did": "",
            "caps": {},
            "peer_id": "",
            "addresses": [],
            "running": False,
            "multiaddr": "",
            "ensembles": [],
        }


def setup_capability(context, instance, user):
    out = exec_cmd(context, instance, f"nunet key new {user}")
    assert_cmd(out)

    out = exec_cmd(context, instance, f"nunet cap new {user}")
    assert_cmd(out)

    out = exec_cmd(context, instance, f"nunet key did {user}")
    assert_cmd(out)

    assert "did:key" in out.stdout, f"stdout: {out.stdout}"
    return out.stdout.strip()


def json_contains(output, keys):
    out = output.strip()
    outjson = json.loads(out)
    for key in keys:
        assert key in outjson, f"{key} not in {outjson}"


def setup_user(context, instance, user):
    init_user_data(context, instance, user)

    user_did = setup_capability(context, instance, user)
    assert user_did != ""

    context.users[user]["did"] = user_did


def setup_node(context, instance, user, node):
    init_node_data(context, instance, user, node)

    node_did = setup_capability(context, instance, node)
    assert node_did != ""

    context.users[user]["nodes"][node]["did"] = node_did
    user_did = context.users[user]["did"]

    out = anchor(context, instance, node, "root", user_did)
    assert_cmd(out)


def save_peer_info(context, instance, user, node):
    out = exec_cmd(
        context,
        instance,
        f"nunet actor cmd --context {node} /dms/node/peers/self",
    )
    assert_cmd(out)
    json_contains(out.stdout, ["id", "listen_addr"])

    out_json = json.loads(out.stdout.strip())

    def find_non_loopback_address():
        addresses = out_json["listen_addr"].split(", ")
        for addr in addresses:
            ip = addr[1:].split("/")[1]
            if not ipaddress.ip_address(ip).is_loopback:
                yield addr

    address = next(find_non_loopback_address(), None)
    assert address is not None, "No valid non-loopback address found"

    node_peer_id = out_json["id"]
    node_multiaddr = f"{address}/p2p/{node_peer_id}"

    context.users[user]["nodes"][node]["peer_id"] = node_peer_id
    context.users[user]["nodes"][node]["multiaddr"] = node_multiaddr


def ensure_node_running(context, user, node):
    """Start a node if it's not already running"""
    if not context.users[user]["nodes"][node]["running"]:
        out = run(context, context.users[user]["nodes"][node]["instance"], user, node)
        assert_cmd(out)
        out = save_peer_info(
            context, context.users[user]["nodes"][node]["instance"], user, node
        )
        assert_cmd(out)


def connect_nodes(context, former_user, former_node, latter_user, latter_node):
    """Connect two nodes if they're different"""
    if former_user != latter_user:
        former_node_instance = context.users[former_user]["nodes"][former_node][
            "instance"
        ]
        latter_multiaddr = context.users[latter_user]["nodes"][latter_node]["multiaddr"]

        out = exec_cmd(
            context,
            former_node_instance,
            f"nunet actor cmd --context {former_node} /dms/node/peers/connect --address {latter_multiaddr}",
        )
        assert_cmd(out)

        output = out.stdout.strip()
        assert "CONNECTED" in output


def set_user_as_root(context, user, node):
    user_did = context.users[user]["did"]
    instance = context.users[user]["nodes"][node]["instance"]

    out = anchor(context, instance, node, "root", user_did)
    assert_cmd(out)


def establish_trust_between_users(
    context, former_user, former_node, latter_user, latter_node
):
    former_user_instance = context.users[former_user]["instance"]
    latter_user_instance = context.users[latter_user]["instance"]

    # users grant each other
    former_grant_token = grant(context, former_user_instance, former_user, latter_user)
    latter_grant_token = grant(context, latter_user_instance, latter_user, former_user)

    # set anchors on users
    out = anchor(
        context, former_user_instance, former_user, "provide", latter_grant_token
    )
    assert_cmd(out)

    out = anchor(
        context, latter_user_instance, latter_user, "provide", former_grant_token
    )
    assert_cmd(out)

    # users delegate to nodes
    former_delegate_token = delegate(
        context, former_user_instance, former_user, former_node
    )
    latter_delegate_token = delegate(
        context, latter_user_instance, latter_user, latter_node
    )

    former_node_instance = context.users[former_user]["nodes"][former_node]["instance"]
    latter_node_instance = context.users[latter_user]["nodes"][latter_node]["instance"]

    # set anchors on nodes
    out = anchor(
        context, former_node_instance, former_node, "provide", former_delegate_token
    )
    assert_cmd(out)

    out = anchor(
        context, latter_node_instance, latter_node, "provide", latter_delegate_token
    )
    assert_cmd(out)


def setup_deployment_capability(context, org, user, node):
    """Set up deployment capability between an organization and a user's node

    Args:
        context: The behave context
        org (str): Organization name
        user (str): Username
        node (str): Node name
    """
    org_instance = context.users[org]["instance"]
    user_instance = context.users[user]["instance"]
    node_instance = context.users[user]["nodes"][node]["instance"]

    # 1. Organization grants deployment capability to user
    org_grant_token = grant(
        context,
        org_instance,
        org,
        user,
    )

    # 2. User anchors the org's token as provide
    out = anchor(
        context,
        user_instance,
        user,
        "provide",
        org_grant_token,
    )
    assert_cmd(out)

    # 3. User grants back to organization for require anchor
    user_grant_token = grant(
        context,
        user_instance,
        user,
        org,
    )

    # 4. Add require anchor to node
    out = anchor(
        context,
        node_instance,
        node,
        "require",
        user_grant_token,
    )
    assert_cmd(out)

    # 5. User delegates to node
    delegate_token = delegate(
        context,
        user_instance,
        user,
        node,
    )

    # 6. Add provide anchor to node
    out = anchor(
        context,
        node_instance,
        node,
        "provide",
        delegate_token,
    )
    assert_cmd(out)


def deploy(context, user, node, dest_user, dest_node, file):
    instance = context.users[user]["nodes"][node]["instance"]
    dest_peer_id = context.users[dest_user]["nodes"][dest_node]["peer_id"]

    select = select_peer(file, "node1", dest_peer_id)
    save_yaml(context, instance, select, file)

    out = exec_cmd(
        context,
        instance,
        f"nunet actor cmd -c {node} /dms/node/deployment/new --spec-file ensembles/{file} --timeout 1h",
    )
    assert_cmd(out)

    out_json = json.loads(out.stdout.strip())
    assert (
        out_json["Status"] == "OK"
    ), f'Failed to create ensemble, status: {out_json["Status"]}'

    ensemble_id = out_json["EnsembleID"]
    context.users[user]["nodes"][node]["ensembles"].append(ensemble_id)


def get_deployments(context, instance, node):
    out = exec_cmd(
        context, instance, f"nunet actor cmd -c {node} /dms/node/deployment/list"
    )
    assert_cmd(out)

    response = json.loads(out.stdout.strip())
    return response.get("Deployments", {})


@backoff.on_predicate(backoff.expo, lambda r: r != "Running", max_time=180)
def ensemble_is_finished(context, instance, node, id):
    out = exec_cmd(
        context,
        instance,
        f"nunet actor cmd -c {node} /dms/node/deployment/status --id {id}",
    )
    assert_cmd(out)

    response = json.loads(out.stdout.strip())
    return response["Status"]


def onboard(context, user, node):
    instance = context.users[user]["nodes"][node]["instance"]
    out = exec_cmd(
        context,
        instance,
        f"nunet actor cmd -c {node} /dms/node/onboarding/onboard -N -C 1 -D 2 -R 2",
    )
    assert_cmd(out)
    print(out.stdout)


def save_yaml(context, instance, yaml_str, filename):
    """Save a YAML string to a file in the LXD instance's ensembles directory"""
    ssh_key = context.config.userdata["ssh_key_file"]
    # Create ensembles directory if it doesn't exist
    exec_command_lxd_instance(instance, "mkdir -p ensembles", ssh_key)
    # Write YAML content to file
    exec_command_lxd_instance(
        instance, f"echo '{yaml_str}' > ensembles/{filename}", ssh_key
    )


def get_deployment_logs(context, user, node, ensemble_id):
    instance = context.users[user]["nodes"][node]["instance"]
    out = exec_cmd(
        context,
        instance,
        f"nunet actor cmd -c {node} /dms/node/deployment/manifest --id {ensemble_id}",
    )
    assert_cmd(out)

    response = json.loads(out.stdout)
    manifest = response["Manifest"]
    allocs = []
    for alloc in manifest["Allocations"].keys():
        allocs.append(alloc)

    logs = {}
    for alloc in allocs:
        out = exec_cmd(
            context,
            instance,
            f"nunet actor cmd -c {node} /dms/node/deployment/logs --id {ensemble_id} --allocation {alloc}",
        )
        assert_cmd(out)

        response = json.loads(out.stdout)
        if response.get("Error"):
            raise Exception(f"Error getting logs: {response['Error']}")

        log_path = response.get("LogsWrittenTo")
        if not log_path:
            raise Exception("No log file path returned")

        out = exec_cmd(context, instance, f"cat {log_path}/stdout.logs")
        assert_cmd(out)

        logs[alloc] = out.stdout

    return logs
