import re
from lib.python.behave.lxd import *
from config import *


@given('dms service is active on the target machine')
def dms_service_active(context):
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = context.instances
    instances_outputs = [
        exec_command_lxd_instance(instance, "sudo systemctl status nunet-dms", ssh_key_file)
        for instance in instances
    ]
    for output in instances_outputs:
        assert "Active: active (running)" in str(output), \
            f"Assertion failed: Expected 'Active: active (running)', but got  {str(output.stdout)}"


@given('dms service is inactive on the target machine')
def dms_service_inactive(context):
    context.dms_service_status_cmd = 'sudo systemctl status nunet-dms'
    assert not is_service_running(context, context.dms_service_status_cmd)


def is_service_running(context, service_command):
    try:
        # Run the service command specified by the user using subprocess
        result = subprocess.run(service_command, shell=True, check=True, capture_output=True, text=True)
        context.command_output = result.stdout.strip()
        # Check if the std output contains the specified text for service status
        if "Active: active (running)" in context.command_output:
            return True
        elif "Active: inactive (dead)" in context.command_output:
            return False
        else:
            print('Server status found other than active (running) or inactive (dead)')
    except subprocess.CalledProcessError as e:
        # Handle errors if the subprocess call fails
        print(f"Error running command: {e}")
        context.command_output = None


@given('device is onboarded on the target machine')
def device_is_onboarded(context):
    assert is_device_onboarded(context), "device onboarding is not done while expecting device onboarding."


@given('device is not onboarded on the target machine')
def device_is_not_onboarded(context):
    assert not is_device_onboarded(context), "device is onboarded while expecting no device onboarding."


def is_device_onboarded(context):
    try:
        ssh_key_file = context.config.userdata["ssh_key_file"]
        instances = context.instances
        instances_outputs = [
            exec_command_lxd_instance(instance, "nunet info", ssh_key_file)
            for instance in instances
        ]
        print(instances_outputs,"instances_outputs")
        for output in instances_outputs:
            if "Error" not in str(output):
                return True
            else:
                return False
    except subprocess.CalledProcessError as e:
        # Handle errors if the subprocess call fails
        print(f"Error running command: {e}")
        context.command_output = None


@when('the user execute the command "{custom_command}" with user input "{user_input}"')
def execute_command(context, custom_command, user_input='empty'):
        ssh_key_file = context.config.userdata["ssh_key_file"]
        instances = context.instances
        context.instances_outputs = [
            exec_command_lxd_instance(instance, custom_command, ssh_key_file, user_input)
            for instance in instances
        ]
        print('context.instances_outputs: ', context.instances_outputs)


@then('the command should contains the string "{expected_string}"')
def verify_output_contains_expected_output(context, expected_string):
    for output in context.instances_outputs:
        if output:
            assert expected_string in str(output), (
                f"Condition not met - peer id of length 46 char starting with Qm "
                "and no special characters in peer id. Actual std output: ",
                output.stdout.decode('utf-8')
            )
        else:
            assert False, "Invalid or empty output in context.instances_outputs"


@then('the command should return the peer id of self')
def step_then_match_conditions(context):
    pattern = re.compile(r'Host ID: Qm[A-Za-z0-9]{44}')
    for output in context.instances_outputs:
        if output and output.stdout:
            peer_id_self = pattern.search(output.stdout.decode('utf-8'))
            assert peer_id_self, ("Condition not met - peer id of length 46 char starting with Qm "
                                  "and no special characters in peer id. Actual std output: ",
                                  output.stdout.decode('utf-8'))
        else:
            assert False, "Invalid or empty output in context.instances_outputs"


@then('the command should return list of peer id')
def step_then_match_conditions(context):
    pattern = re.compile(r'^Qm[0-9a-zA-Z]{44}$', re.MULTILINE)
    for output in context.instances_outputs:
        if output and output.stdout:
            peer_list = pattern.findall(output.stdout.decode('utf-8'))
            assert peer_list, ("Condition not met - peer id of length 46 char starting with Qm "
                               "and no special characters in peer id. Actual std output: ",
                               output.stdout.decode('utf-8')
                               )
        else:
            assert False, "Invalid or empty output in context.instances_outputs"


@then('it should match the specified format for line "{line}"')
def step_then_match_format_for_line(context, line):
    for output in context.instances_outputs:
        if output and output.stdout:
            expected_output_pattern = re.compile(re.escape(line))
            match = expected_output_pattern.search(output.stdout.decode('utf-8'))
            assert match, f"Line does not match expected format. Actual line:\n{line}"
        else:
            assert False, "Invalid or empty output in context.instances_outputs"


@when('the nunet command output is processed to get the system parameters')
def step_process_output(context):
    context.nunet_full_system_capacity = []
    for output in context.instances_outputs:
        if output and output.stdout:
            context.nunet_full_system_capacity.append(extract_data(output.stdout.decode('utf-8')))
        else:
            assert False, "Invalid or empty output in context.instances_outputs"


def extract_data(std_output):
    # Extract data using regular expressions for the new format
    pattern = re.compile(r'\|\s+Full\s+\|\s+(\d+)\s+\|\s+(\d+)\s+\|\s+(\d+)\s+\|')
    match = pattern.search(std_output)
    if match:
        memory, cpu, cores = map(int, match.groups())
        return {"cpu": cpu, "memory": memory, "cores": cores}
    else:
        raise ValueError("Unable to extract data from the standard output")


@when('I get the system full capacity using linux native commands')
def step_get_system_information(context):
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = context.instances
    context.full_cpu_capacity = []
    context.linux_system_full_capacity = []
    for instance in instances:
        get_cpu_cores_command = "lscpu | grep '^CPU(s):' | awk '{print $2}'"
        total_cores_response = exec_command_lxd_instance(instance, get_cpu_cores_command, ssh_key_file)
        context.total_cores = int(total_cores_response.stdout.strip())

        lscpu_version_command = "lscpu --version"
        lscpu_version = exec_command_lxd_instance(instance, lscpu_version_command, ssh_key_file)
        output = lscpu_version.stdout.strip()

        if any(version in str(output) for version in ('2.37.2', '2.39.3', '2.38.1')):
            get_cpu_command = """lscpu -p=+MHZ | awk -F, '{sum+=$NF} END {printf "%d\\n", sum}'"""
            cpu_response = exec_command_lxd_instance(instance, get_cpu_command, ssh_key_file)
            context.full_cpu_capacity = int(cpu_response.stdout.strip())
        else:
            temp_result1 = exec_command_lxd_instance(instance, 'lscpu', ssh_key_file)
            temp_result = temp_result1.stdout.strip()
            if 'CPU max MHz' in str(temp_result):
                get_cpu_command = "lscpu | grep 'CPU max MHz' | awk '{print $4}'"
            else:
                get_cpu_command = "lscpu | grep 'CPU MHz' | awk '{print $3}'"

            cpu_response = exec_command_lxd_instance(instance, get_cpu_command, ssh_key_file)
            context.cpu = cpu_response.stdout.strip()
            context.full_cpu_capacity = context.total_cores * float(context.cpu)

        get_memory_command = "free -m | grep 'Mem:' | awk '{print $2}'"
        result = exec_command_lxd_instance(instance, get_memory_command, ssh_key_file)
        context.memory = int(result.stdout.strip())
        context.linux_system_full_capacity.append({
            "cpu": context.full_cpu_capacity,
            "memory": context.memory,
            "cores": context.total_cores
            # "total_cores": context.total_cores
        })

        print('linux_system_full_capacity: ', context.linux_system_full_capacity)


@then('nunet command result should match with native linux command result')
def step_compare_absolute_values(context):
    print(context.nunet_full_system_capacity, "context.nunet_full_system_capacity")
    for i in range(len(context.nunet_full_system_capacity)):
        extracted_data_memory = context.nunet_full_system_capacity[i]['memory']
        linux_system_memory = round(float(context.linux_system_full_capacity[i]['memory']), 1)
        for key in context.nunet_full_system_capacity[i].keys():
            extracted_data_value = context.nunet_full_system_capacity[i][key]
            linux_system_value = context.linux_system_full_capacity[i][key]
            if key == 'memory':
                # Compare the absolute values for memory
                assert round(extracted_data_memory) == round(linux_system_memory), (f'memory found by nunet full system '
                                                                                    f'capacity command is : '
                                                                                    f'{round(extracted_data_memory)} while memory '
                                                                                    f'found by linux command is : '
                                                                                    f'{round(linux_system_memory)}')
            else:
                # Compare the absolute values for other key items
                assert round(extracted_data_value) == round(linux_system_value), (f'{key} found by nunet full system '
                                                                                  f'capacity command is : '
                                                                                  f'{round(extracted_data_value)} while {key} '
                                                                                  f'found by linux command is : '
                                                                                  f'{round(linux_system_value)}')


@then('the command should exit with message "{message}"')
def verify_exit_message(context, message):
    for output in context.instances_outputs:
        if output:
            assert message.lower() in str(output).lower(), (
                f"Expected '{message.lower()}' in output, but got: {output}"
            )
        else:
            assert False, "Invalid or empty output in context.instances_outputs"


def calculate_capacity_percentage(percentage, max_capacity):
    return int((percentage / 100) * max_capacity)


def run_nunet_onboard(context, memory_percent, cpu_percent, user_input):
    command = f"nunet onboard -m {memory_percent} -c {cpu_percent} -n nunet-team -a {WALLET_ADD} -l"
    print('command: ', command)
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = context.instances
    context.instances_outputs = [
        exec_command_lxd_instance(instance, command, ssh_key_file, user_input)
        for instance in instances
    ]


@when('I run nunet onboard command with {memory_percent:d}% memory and {cpu_percent:d}% CPU with '
      'user input "{user_input}"')
def step_run_nunet_onboard_with_capacity(context, memory_percent, cpu_percent, user_input='empty'):
    for system_capacity in context.linux_system_full_capacity:
        full_memory_capacity = float(system_capacity["memory"])
        full_cpu_capacity = system_capacity["cpu"]

        memory_percent_value = calculate_capacity_percentage(memory_percent, full_memory_capacity)
        cpu_percent_value = calculate_capacity_percentage(cpu_percent, full_cpu_capacity)

        run_nunet_onboard(context, memory_percent_value, cpu_percent_value, user_input)
