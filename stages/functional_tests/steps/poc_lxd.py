import subprocess
import json
from sshtunnel import SSHTunnelForwarder
import requests

from behave import given, then, when

from config import *


WALLET_ADDRESS = "addr1q8u8f749rae9fctvgpfay2u808x636p974ecvumryx80hggewes7xswxzgk4ravjvdtszf3ss6zh4hkqykm8gjh79a4qz9cy7t"


def get_dms_on_lxd_instances(inventory_file):
    with open(inventory_file) as file_fp:
        instances = [line.strip() for line in file_fp.readlines()]

    return instances


def lxd_instance_bash_exec(instance, command, ssh_key):
    try:
        response = subprocess.run(
            [
                "ssh",
                "-o",
                "StrictHostKeyChecking=no",
                "-o",
                "IdentitiesOnly=yes",
                "-o",
                "PubKeyAuthentication=yes",
                "-i",
                ssh_key,
                f"root@{instance}",
                "--",
                command,
            ],
            capture_output=True,
            check=True,
        )
        return response
    except subprocess.CalledProcessError as e:
        print("ssh stdout:")
        print(e.stdout.decode())
        print("ssh stderr:")
        print(e.stderr.decode())
        raise e


@given("dms lxd instances are up")
def verify_dms_on_lxd_up(context):
    inventory_file = context.config.userdata["inventory_file"]
    instances = get_dms_on_lxd_instances(inventory_file)
    context.instances = instances
    assert len(instances) > 0, f"{len(instances)} is not 0"


@given("nunet is available on all instances")
def verify_nunet_installed_on_instances(context):
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = context.instances
    instances_outputs = [
        lxd_instance_bash_exec(instance, "nunet --version", ssh_key_file)
        for instance in instances
    ]
    for output in instances_outputs:
        assert "nunet" in str(
            output.stdout
        ), f"nunet not in stdout {str(output.stdout)}"


@when("nunet onboard is executed in all instances")
def onboard_nunet_on_all_instances(context):
    inventory_file = context.config.userdata["inventory_file"]
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = get_dms_on_lxd_instances(inventory_file)
    instances_outputs = [
        lxd_instance_bash_exec(
            instance,
            f"nunet onboard -m 2000 -c 1500 -n nunet-test -l -a {WALLET_ADDRESS}",
            ssh_key_file,
        )
        for instance in instances
    ]
    context.onboard_outputs = instances_outputs

    assert all(output.returncode == 0 for output in instances_outputs)


@then("all instances should onboard")
def check_all_instances_onboarded(context):
    for output in context.onboard_outputs:
        stdout = output.stdout.decode()
        assert (
            "Successful" in stdout or "Sucessfully" in stdout
        ), f"{stdout} not successful"


@when("listing all peers in each instance")
def list_peers_in_each_instance(context):
    inventory_file = context.config.userdata["inventory_file"]
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = get_dms_on_lxd_instances(inventory_file)
    context.instance_ids = [
        lxd_instance_bash_exec(
            instance,
            "nunet peer self | grep -Ei 'Host ID: ' | cut -d ':' -f 2",
            ssh_key_file,
        ).stdout.decode()
        for instance in instances
    ]
    context.peers_lists = [
        lxd_instance_bash_exec(
            instance, "nunet peer list", ssh_key_file
        ).stdout.decode()
        for instance in instances
    ]


@then("every other instance id should appear in the list")
def check_peers_list(context):
    for peers_list, ids_to_test in zip(
        context.peers_lists,
        [
            [e for i_in, e in enumerate(context.instance_ids) if i_in != i_out]
            for i_out in range(len(context.instance_ids))
        ],
    ):
        for instance_id in ids_to_test:
            assert (
                instance_id in peers_list
            ), f"{instance_id} not in peers list {peers_list}"


#     When querying "/api/v1/device/status"
#     Then status code must be 200
#     And response body should be
#       """
#       {"device":"onboarded"}
#       """
@when('sending a get to "{endpoint}"')
def send_get_to_endpoint(context, endpoint):
    inventory_file = context.config.userdata["inventory_file"]
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = get_dms_on_lxd_instances(inventory_file)
    context.responses = {}
    for instance in instances:
        server = SSHTunnelForwarder(
            instance,
            ssh_username="root",
            ssh_pkey=ssh_key_file,
            remote_bind_address=("127.0.0.1", 9999),
        )

        server.start()

        local_endpoint = f"http://localhost:{server.local_bind_port}{endpoint}"
        context.responses[instance] = requests.get(local_endpoint)

        server.stop()


@then("status code must be {expected_status_code}")
def verify_status_code(context, expected_status_code):
    for instance, response in context.responses.items():
        assert response.status_code == int(
            expected_status_code
        ), f"{instance} response {response.status_code} not {expected_status_code}"


@then("response body should be")
def verify_response_body(context):
    for instance, response in context.responses.items():
        expected_json = json.loads(context.text)
        response_json = response.json()

        print(expected_json)
        print(response_json)

        expected_status = expected_json["online"]
        actual_status = response_json.get("online")
        assert (
            expected_status == actual_status
        ), f"{instance} expected status {expected_status} not {actual_status}, error message {response_json.get('error')}"


@when("nunet offboard is executed in all instances")
def offboard_nunet_on_all_instances(context):
    inventory_file = context.config.userdata["inventory_file"]
    ssh_key_file = context.config.userdata["ssh_key_file"]
    instances = get_dms_on_lxd_instances(inventory_file)
    instances_outputs = [
        lxd_instance_bash_exec(
            instance,
            "yes | nunet offboard",
            ssh_key_file,
        )
        for instance in instances
    ]
    context.onboard_outputs = instances_outputs

    assert all(output.returncode == 0 for output in instances_outputs)


@then("all instances should offboard")
def check_all_instances_offboarded(context):
    for output in context.onboard_outputs:
        stdout = output.stdout.decode()
        assert "device successfully offboarded" in stdout, f"{stdout} not successful"
