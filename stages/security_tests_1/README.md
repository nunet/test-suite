# security_tests_1

Automated security testing (using third party tools) does not need live environments or a testnet, i.e. can be run on the static repository code.

Implemented: https://gitlab.com/nunet/nunet-infra/-/blob/develop/ci/templates/Jobs/Security-Tests-1.gitlab-ci.yml